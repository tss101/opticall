/*
 *  filtering.cpp
 *  
 *
 *  Created by Tejas Shah on 15/11/2011.
 *  Copyright 2011 WTSI. 
 *
 */


#include <algorithm>
#include "inclusions.h"


using namespace std;
using namespace boost;
using namespace Eigen;

/*********Counting the lines in a file********/


unsigned int fileRead( istream & is, vector <char> & buff ) {
	is.read( &buff[0], buff.size() );
	return is.gcount();
}

unsigned int countLines( const vector <char> & buff, int sz ) {
	int newlines = 0;
	const char * p = &buff[0];
	for ( int i = 0; i < sz; i++ ) {
		if ( p[i] == '\n' ) {
			newlines++;
		}
	}
	return newlines;
}

unsigned int snpCount(const char * filename, bool header) {
    time_t now = time(0);
	
	//cout << "buffer\n";
	const int SZ = 1024 * 1024;
	vector <char> buff( SZ );
	ifstream ifs( filename );
	int n = 0;
	while( int cc = fileRead( ifs, buff ) ) {
		n += countLines( buff, cc );
	}
	//cout << n << endl;
    //cout << time(0) - now << endl;
	
	if (header) {
		return n - 1;
	}
	return n;
	
}



/***********************Parse a line to get the intensities***************/





vector<string> intensitiesFromLine(string line, vector < vector <string>  > &snpinfo){
	
	
	vector<string> linedata;
	linedata.reserve(6000);    // make room for 10 elements
	
	trim(line);
	
	split(linedata, line, boost::is_any_of("\t "), token_compress_on);
	
	//cout << "bits of line data " << linedata[5] << " " << linedata[linedata.size()-1] << " " << linedata.size()  << endl;
	
	//cout << linedata.size() << "\n";
	vector<string> individs(&linedata[0]+3,&linedata[0]+linedata.size());
	vector<string> snpdesc(&linedata[0],&linedata[0]+3);
	snpinfo.push_back(snpdesc);
	return individs;
	
}




/*****************Get Header Info**************/

vector<string> readHeaderInfoFromIntFile(const char * fname) {
	
	//declare a vector to hold the data
	vector<string> individualsx2;
	vector< vector<string> > junkheaderdetail;
	
	string header;
	ifstream myfile (fname);
	if (myfile.is_open())
	{
		if ( myfile.good() )
		{
			getline (myfile,header);
			//cout << line << endl;
			individualsx2 = intensitiesFromLine(header,junkheaderdetail);
		}
		myfile.close();
	}
	else cout << "Unable to open file";
	
	vector<string> individuals;
	
	for(vector< string >::iterator ind_iter = individualsx2.begin();
		ind_iter != individualsx2.end(); ++++ind_iter){
		individuals.push_back((*ind_iter).substr(0,(*ind_iter).length()-1));
	}
	
	
	/*
	 for(vector< string >::iterator ind_iter = individuals.begin();
	 ind_iter != individuals.end(); ++ind_iter){
	 cout << *ind_iter << "\n";
	 }	
	 
	 cout << individuals.size() << "\n";
	 */
	return individuals;
	
	//provide the header info
}





/*********************Read a vblock from file and return *****************/

MatrixXd readVblockFromIntFile(ifstream & myfile, int vertical_block_size, int numsamples, vector < vector <string>  > &snpinfo, int offset) {
	string line;
	//vector<vector<string>> flat_intensities
	MatrixXd vblock(vertical_block_size,(numsamples)*2); 
	vector<string> intensities;
	
	//ifstream myfile (fname);
	if (myfile.is_open())
	{
		int snpcounter = 0;
		while ( myfile.good() && snpcounter < vertical_block_size)
		{
			getline (myfile,line);
			//cout << line << endl;
			intensities = intensitiesFromLine(line,snpinfo);
			//now set the part of the matrix with the intensities
			for (int j = 0+2*offset; j < (offset+numsamples)*2; j = j+2 ) {
				
				//cout << "starting next step of loop for offset " << offset << "with counter " << snpcounter << "and j " << j << endl;
				//cout << intensities.size() << endl;
				
				//cout << vblock(snpcounter,j - 2*offset) << endl;
				
				//cout << "didn't crap out" << endl;
				
				if (intensities[j] == "NaN")
				{
					//cout << "have an NaN in the dataset" << endl;
					vblock(snpcounter,j - 2*offset) = numeric_limits<double>::quiet_NaN();
					vblock(snpcounter,j+1 - 2*offset) = numeric_limits<double>::quiet_NaN();
				}
				
				vblock(snpcounter,j - 2*offset) = atof( intensities[j].c_str() );
				vblock(snpcounter,j+1 -2*offset) = atof( intensities[j+1].c_str() );
				
				//cout << "ccounter" << snpcounter << " vblock is " << vblock(snpcounter,j - 2*offset) << " " << vblock(snpcounter,j+1 -2*offset) << endl;
				
			}
			snpcounter++;
		}
		//myfile.close();
	}
	
	//cout << "\nblock:\n" << vblock.row(2) << endl;
	return vblock;
	//MatrixXi Vblock(vertical_block_size,numsamples); 	
	
	
	//else cout << "Unable to open file";
}


/********************* Read gender file ******************************/

vector<int> readInfoFile(string fname, vector<string> allsamples, MatrixXi &samplegenders, MatrixXi &samplebatches) {

	string line;
	
	vector<string> infosampleids;
	vector<int> infosamplegender;
	vector<int> infosampleexclude;
	vector<int> infosamplebatch;
	
	ifstream myfile (fname.c_str());
	
	if (myfile.is_open())
	{
		int samplecounter = 0;
		while ( myfile.is_open()  && myfile.good() )
		{
			getline (myfile,line);
			//split the file up and save it

			
			vector<string> linedata;
			linedata.reserve(3000);    // make room for 5 elements
			trim(line);
			split(linedata, line, boost::is_any_of("\t "), token_compress_on);
			
			//cout << "gotten line " << linedata.size() << endl;
			
			if (linedata.size() < 3) 
			{
				continue;
			}
			
			//linedata[0] is the sampleid, linedata[1] is the sex, linedata[2] is the exclusion flag, linedata[3] is the batch
			infosampleids.push_back(linedata[0]);
			infosamplegender.push_back(atoi(linedata[1].c_str()));
			infosampleexclude.push_back(atoi(linedata[2].c_str()));
            infosamplebatch.push_back(atoi(linedata[3].c_str()));
			
			
		}
	}
	
	myfile.close();
	
	//cout << "gotten lines" << endl;
	
	vector<int> exclusion_indices;
			
	//then compare with the sample list
	for (int i = 0; i < allsamples.size(); i++)
	{
		samplegenders(i,0) = 0;
        samplebatches(i,0) = 0;
		for (int j = 0; j < infosampleids.size(); j++ )
		{
			if ( allsamples[i] == infosampleids[j] && infosampleexclude[j] == 1 &&  find (exclusion_indices.begin(), exclusion_indices.end(), i)  ==   exclusion_indices.end()  ) {
				exclusion_indices.push_back(i);
			}
			if ( allsamples[i] == infosampleids[j]  ) {
				samplegenders(i,0) = infosamplegender[j];
                samplebatches(i,0) = infosamplebatch[j];
			}
		}
	}
	
	//so exclusion list has been populated
	return exclusion_indices;
	
}


/********************* Combine two lists of indices to be excluded into one *************/

vector<int> combineExclusionLists(vector<int> list1, vector<int> list2)
{	
	vector<int> outputlist = list1;
	for (int i = 0; i < list2.size() ; i++)
	{
		if (outputlist.size() == 0  ||  find (outputlist.begin(), outputlist.end(), list2[i])  ==   outputlist.end() )
		{
			outputlist.push_back(list2[i]);
		}
	}
	return outputlist;
}


/************************ returns a list of individualstobecalled i.e. individuals who have not been excluded  ***********************************/

vector<string> individualsForCalling(vector<string> individuals, vector<int> excluded)
{
	vector<string> newindlist;
	for (int i = 0; i < individuals.size() ; i++)
	{
		if (excluded.size() == 0 ||  find (excluded.begin(), excluded.end(), i)  ==   excluded.end()  )
		{
			newindlist.push_back(individuals[i]);
		}
	}
	return newindlist;
	
}




/********************* Sorting function for random numbers*******/

bool lessthanfunction (int i, int j) { return (i<j); }

bool longlessthanfunction (long int i, long int j) { return (i<j); }



/*************Find samples with mean intensities more than numsds away from the mean ******/

vector<int> findIntensityOutliers(string fname, int snpcount, vector<string> individuals,double numsds, vector<int> exclude_list) {
	
	
	
	MatrixXd sumdistances = MatrixXd::Zero(individuals.size(),1);
	MatrixXd countdistances = MatrixXd::Zero(individuals.size(),1);

	//figure out the number of individuals to get on a run through the file assuming 500MB of RAM
	int indsperrun = floor( 1000000 / (2*snpcount)  );
	int indsdone = 0;
	
	cout << "removing outliers based on mean intensity" << endl;
	

	
	while (indsdone < individuals.size())
	{
	
		ifstream intensityfile (fname.c_str());
		if (intensityfile.is_open()  && intensityfile.good())
		{
			string header;
			getline(intensityfile,header);
		}
		

		
		int numsamples = min( (int) indsperrun, (int) individuals.size() - indsdone );  //minimum of indsperrun and (individuals.size() - indsdone)
		int offset = indsdone;
		
		vector < vector< string > > snpinfo;
		MatrixXd block = readVblockFromIntFile(intensityfile,snpcount,numsamples,snpinfo, offset   );	//get intensity data from the file
		
		//cout << block << endl;
		//cout << block.rows() << " " << block.cols() << endl;
		
		intensityfile.close();

		
		for (int snpt = 0; snpt < snpcount; snpt++)
		{
			for(int ind = 0; ind < numsamples; ind++)
			{
				double distance = sqrt( block(snpt, 2*ind)*block(snpt, 2*ind) + block(snpt, 2*ind+1)*block(snpt, 2*ind+1)  );
				if (!isnan(distance) && !isinf(distance)  )
				{
					sumdistances(ind+offset,0) = sumdistances(ind+offset,0) + distance;
					countdistances(ind+offset,0)++;
				}
			}
		}
		

		
		indsdone = indsdone + numsamples;
		
		
	}
	
	

	MatrixXd meandistances = sumdistances.array() / countdistances.array();
	//Next need to account for NANs - before calculating Stdev
	double sumofmeandists = 0;
	double sumofmeandistssq = 0;
	int count = 0;
	
	for (int i = 0; i < meandistances.rows(); i++)
	{
		double mdist = meandistances(i,0);
		if (!isnan(mdist) && !isinf(mdist)  && (exclude_list.size() == 0 || find (exclude_list.begin(), exclude_list.end(), i)  ==   exclude_list.end() )  )
		{
			count++;
			sumofmeandists = sumofmeandists + mdist;
			sumofmeandistssq = sumofmeandistssq + mdist*mdist;
		}
	}
	
	double meandist = sumofmeandists/count;
	double sddist = sqrt( sumofmeandistssq/count - meandist*meandist );
	
	double lower_bound = meandist  - numsds*sddist;
	double upper_bound = meandist + numsds*sddist;
	
	vector<int> outlying_samples;
	
	
	cout << meandist << " " << sddist << endl;
	cout << "end mean distances" << endl;
	
	for (int i = 0; i < meandistances.rows(); i++)
	{
		double mdist = meandistances(i,0);
		if (  ( isnan(mdist) || isinf(mdist) || mdist < lower_bound || mdist > upper_bound )  && (exclude_list.size() == 0 || find (exclude_list.begin(), exclude_list.end(), i)  ==   exclude_list.end() )   )
		{
			outlying_samples.push_back(i);
			cout << "excluding mean intensity outlier " << i << " with sampleid " << individuals[i]  << " and mdist " << mdist << endl;
		}
	}
	
	
	
	return outlying_samples;
}






/***********Create a random sample from the intensity data**********/

MatrixXd fetchRandomIntensities(string fname, int sample_block_size, int snpcount, vector<string> individuals, vector<int> initial_outliers) {
        cout << "fetching random intensities" << endl;
	
	vector < vector< string > > snpinfo;
	
	ifstream intensityfile (fname.c_str());
	if (intensityfile.is_open()  && intensityfile.good())
	{
		string header;
		getline(intensityfile,header);
	}
        //cout << "num SNPs from block is " << snpcount  << endl;
        //cout << "num inds from block is " << individuals.size()  << endl;
        //cout << "product is " << snpcount*individuals.size()  << endl;
        //cout << "min is " << min<long int>(sample_block_size,snpcount*individuals.size())  << endl;
	/**
	 * STEP 1: Create one block mixture model - by sampling the file once
	 */
	//collect data
	MatrixXd sample(min<long int>(sample_block_size,snpcount*individuals.size()),2);
	vector<long int> sampling_indices;

	//cout << "getting ready to smaple" << endl;
	//printf("%s\n", sample_block_size >= snpcount*individuals.size()?"true":"false");
	//first check if there's actually enough points to take a big sample
	if (sample_block_size >= snpcount*individuals.size())
	{
		//cout << " not enough points for sample " << endl;
		for (long int i = 0; i < snpcount*individuals.size(); i++) {
			//TODO: Need to consider the excluded sample indices
			bool excluded = false;
			for (int outlind = 0; outlind < initial_outliers.size(); outlind++ )
			{
				if ((i - initial_outliers[outlind])%individuals.size() == 0 ) 
				{
					excluded = true;
				}
			}
			
			if (!excluded)
			{
				sampling_indices.push_back(i);
			}
			
		}
		
		
	}
	else {
		//cout << " enough points for random sample " << endl;
		//select a bunch of random numbers
		srand(time(0));
		while (sampling_indices.size() < sample_block_size) {
			float randnum = (float)rand()/(float)RAND_MAX;
			//long int val = floor(randnum*snpcount*individuals.size());
			long int val = lrint( randnum*snpcount*individuals.size() );
			//cout << "val is " << val << endl;
			
			if (val == snpcount*individuals.size()) {
				//slight fudge if the random number chosen is exactly 1.0
				val = val -1 ;
			}
			
			//TODO: Need to consider the excluded sample indices
			bool excluded = false;
			for (int outlind = 0; outlind < initial_outliers.size(); outlind++ )
			{
				if ((val - initial_outliers[outlind])%individuals.size() == 0 ) 
				{
					excluded = true;
					//cout << "exclusion " << val << "belongs to " << initial_outliers[outlind] << endl;
				}
			}
			
			//our random value is not in the list - add it
			if (!excluded && find (sampling_indices.begin(), sampling_indices.end(), val)  ==   sampling_indices.end() ) {
				//cout << "pushing back val " <<  val << endl;
				sampling_indices.push_back(val);
			}
		}
		//ok so now we have the required number of random values - sort them
		sort (sampling_indices.begin(), sampling_indices.end(), longlessthanfunction);
	}

	//cout << sampling_indices[0] << " " << sampling_indices[1] << endl;
	//for (int i=0; i<sampling_indices.size();i++){cout << sampling_indices[i] << endl;}
	//cout << sampling_indices.size() << endl;
	//cout << "done printing" << endl;
	//and now finally we fetch the sampled file
	int counter = 0;
	for (int snpt = 0; snpt < snpcount; snpt++)
	{
		//cout << "getting into loop" << endl;
		MatrixXd block = readVblockFromIntFile(intensityfile,1,individuals.size(),snpinfo,0);	//read one line from the file
		//now put the block into a sample
	
		//cout << snpinfo[snpt][0] << endl;
	
		for (int ind = 0; ind < individuals.size(); ind++)
		{

			long int curr_pt_index = snpt*individuals.size() + ind;
			if (curr_pt_index == sampling_indices[0])
			{
				sampling_indices.erase(sampling_indices.begin());
				sample(counter,0) = block(0,ind*2);
				sample(counter,1) = block(0,ind*2+1);
				//cout << "got point " << sample(counter,0) << " " << sample(counter,1) << endl;
				counter = counter + 1;
			}
		}
	}


	intensityfile.close();
	return sample;

}




