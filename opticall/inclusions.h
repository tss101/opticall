/*
 *  inclusions.h
 *  
 *
 *  Created by Tejas Shah on 24/11/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */



#ifndef INCLUSIONS_H
#define INCLUSIONS_H

#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <ctime>
#include <string>
#include <limits>
#include <sstream>

#include <time.h>

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/normal.hpp>

#include <Eigen/Core>
#include <Eigen/LU>

#endif