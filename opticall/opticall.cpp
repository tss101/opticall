// simple_example_2.cpp


#include "inclusions.h"
#include "filtering.h"

#include "kmpp/KMeans.h"



using namespace std;
using namespace boost;
using namespace Eigen;


double G_CALL_THRESHOLD = 0.7;


/*********Doesn't C++ have these?******/

int roundtoint(double r) {
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
}







/*********Functions the Eigen library should have*****/



vector<int>	argsort(MatrixXd matrix,int axis, int index, bool descending) {
	//The argsort function that takes in a matrix, and an axis - 0 (row), 1(column), with an index
	//then returns a list of indices based on the sorted values

	//we just need to consider the relevant block of the matrix


	MatrixXd sortingsegment;
	if (axis == 0) {
		sortingsegment = matrix.row(index).transpose();
	}
	else {
		sortingsegment = matrix.col(index);
	}

	//cout << "sorting segment " << endl;
	//cout << sortingsegment << endl;

	vector<int>argsorts;
	for (int i = 0; i < sortingsegment.rows(); i++)
	{
		argsorts.push_back(i);
	}

	//cout << argsorts.size() << endl;

	//got the bit to be sorted - now I need to sort it
	//there's waaay better algorithms than bubblesort - but I'll sort this later...no pun intended
	bool swapped = false;
	do
	{
		swapped = false;
		for (int j = 0; j < sortingsegment.rows()-1; j++)
		{
			if (descending && sortingsegment(argsorts[j],0) < sortingsegment(argsorts[j+1],0)     ) {
				int tmpargind = argsorts[j+1];
				argsorts[j+1]=argsorts[j];
				argsorts[j]=tmpargind;
				swapped = true;
			}
			else if (!descending && sortingsegment(argsorts[j],0) > sortingsegment(argsorts[j+1],0) ) {
				int tmpargind = argsorts[j+1];
				argsorts[j+1]=argsorts[j];
				argsorts[j]=tmpargind;
				swapped = true;
			}
		}
	} while (swapped);

	return argsorts;

}


MatrixXd sortedmatrix(MatrixXd matrix, int axis, vector<int>argsorts) {

	MatrixXd sortedoutput = MatrixXd::Zero(matrix.rows(), matrix.cols());
	int loopnumber = matrix.cols();
	if (axis == 1){
		loopnumber = matrix.rows();
	}
	for (int i = 0; i < loopnumber; i++)
	{
		if (axis == 0) {
			sortedoutput.col(i) = matrix.col(argsorts[i]);
		}
		else {
			sortedoutput.row(i) =	matrix.row(argsorts[i]);
		}
	}

	return sortedoutput;
}


/********************* Approx quantile fn. for low p-vals**************/

//Approximation to normal zscore 
//from http://www.johndcook.com/normal_cdf_inverse.html
//originally from Abramowitz and Stegun
//Handbook of Mathematical Functions: with Formulas, Graphs, and Mathematical Tables

double RationalApproximation(double t)
{
    cout << "t is " << t << endl;
    double c0 = 2.515517;
    double c1 = 0.802853;
    double c2 = 0.010328;
    double d1 = 1.432788;
    double d2 = 0.189269;
    double d3 = 0.001308;
    
    double numerator = c0 + c1*t + c2*t*t;
    double denomenator = ((double)1.0) + d1*t + d2*t*t + d3*t*t*t;
    
    return  t-numerator/denomenator;

}


double approx_quantile(double p)
{
    //See article above for explanation of this section.
    //stop the INFS and NANS
    //cout << "p is"<< scientific << p << endl;
    cout << "abs(-2.0*log(p)) " << abs(-2.0*log(p))  << endl;
    if (p > 0.99999999) { p = 0.99999999; }
    if (p < 0.5)
    {
	    // F^-1(p) = - G^-1(p)
	    return -RationalApproximation( sqrt(abs(((double)-2.0)*log(p))) );
    }
    else
    {
	    // F^-1(p) = G^-1(1-p)
	    return RationalApproximation( sqrt(abs(((double)-2.0)*log(1-p))) );
    }
}



double approx_right_tail_cdf(double z)
{
    
    //Approximation to right normal tail integral 
    //from "A UNIFORM APPROXIMATION TO THE RIGHT NORMAL TAIL INTEGRAL"
    //W. BRYC
    //formula (11) By Hart
    //A Close Approximation Related to the Error Function
    //http://www.ams.org/journals/mcom/1966-20-096/S0025-5718-1966-0203907-1/S0025-5718-1966-0203907-1.pdf
    

    
    double p0 = sqrt(M_PI/2);
    double a = (1+sqrt(1-2*M_PI*M_PI+6*M_PI)) / (2*M_PI);
    double b = 2*M_PI*a*a;
    
    double multiplier = exp(-1*z*z/2)/(sqrt(2*M_PI)*z);
    double numerator = sqrt(1+b*z*z )/(1+a*z*z );
    
    double denomenator =  p0*z + sqrt( p0*p0*z*z + exp(-1*z*z/2)*numerator   );
    
    

    
    return multiplier*(1-numerator/denomenator);
    
}





/********************* Get Variances from the data *******************/

double logcoeff(double coeff) {
	return log(coeff);

}

double logcoeffpl1(double coeff) {
	return log(coeff+1);
}

double expcoeff(double coeff) {
	return exp(coeff);
}

double cleannan(double coeff) {
	if (isinf(coeff) ||isnan(coeff)) {
		return 0;
	}
	else {
		return coeff;
	}
}

MatrixXd sd_of_data(const MatrixXd &line_data) {
	MatrixXd mu_data = line_data.colwise().sum()/line_data.rows();
	MatrixXd mu_datasq = (line_data.array()*line_data.array()).colwise().sum()/line_data.rows();
	MatrixXd var_data = mu_datasq.array()-(mu_data.array()*mu_data.array());
	MatrixXd sd_data = var_data.array().sqrt();
	return sd_data;
}

MatrixXd transform_data(const MatrixXd  &line_data) {
	MatrixXd transf_data(line_data.rows(), line_data.cols());
	transf_data.col(0) = (line_data.col(0) - line_data.col(1)).array()/(line_data.col(0) + line_data.col(1)).array();
	transf_data.col(0) = transf_data.col(0).unaryExpr(ptr_fun(cleannan));

	//cout << ((line_data.col(0) + line_data.col(1)).array() + 1) << endl;

	transf_data.col(1) =  ((line_data.col(0) + line_data.col(1)).array() + 1);
	transf_data.col(1) = transf_data.col(1).unaryExpr(ptr_fun(logcoeff));

	//cout << transf_data.col(0) << endl;

	return transf_data;
}



MatrixXd untransform_data(const MatrixXd &line_data) {
	MatrixXd untransf_data(line_data.rows(), line_data.cols());
	untransf_data.col(0) = 0.5*(line_data.col(0).array() + 1).array()*( line_data.col(1).unaryExpr(ptr_fun(expcoeff)).array() -1 ).array();
	untransf_data.col(1) = 0.5*(1-line_data.col(0).array()).array()*( line_data.col(1).unaryExpr(ptr_fun(expcoeff)).array() -1 ).array();
	return untransf_data;
}


/************************Nan cleaning functions*****************************/



MatrixXd strip_outliers(const MatrixXd &inputdata, vector<int> &exc_indices, double &outlier_range, bool set_outlier_range) {
	
	vector<int> num_indices;
	
	MatrixXd distances = inputdata.rowwise().norm();
	
	double mean_dist = distances.mean();
	double sd_dist = sd_of_data(distances)(0,0);
	
	
	
	if (set_outlier_range)
	{
		outlier_range = 4.0*sd_dist;
	}
	//else
	//{
	//	mean_dist = 0; //because in this case the outlier range includes the mean dist 
	//}

	
	
	for (int i = 0; i < distances.rows(); i++)
	{
		if ( distances(i,0) > mean_dist +  outlier_range )
		{
			exc_indices.push_back(i);
			//cout << "excluding " << inputdata.row(i) << " with distance: " << inputdata.row(i).norm() << endl;
		}
		else
		{
			num_indices.push_back(i);
		}
	}
	
	MatrixXd num_data(num_indices.size(),inputdata.cols());
	for (int i =0; i < num_indices.size(); i++)
	{
		num_data.row(i) = inputdata.row(num_indices[i]);
	}
	
	return num_data;
	
	
	
}



MatrixXd strip_nans(const MatrixXd &inputdata, vector<int> &nan_indices, bool nonorm, double &outlier_range, bool set_outlier_range, vector<int> &exclude_list, vector<int> &excluded_samples, MatrixXi &genders, MatrixXi &batches) {

	vector<int> num_indices;
	
	vector<int> infofile_excluded_indices;

	for (int i = 0; i < inputdata.rows(); i++)
	{
		if (excluded_samples.size() > 0  && find (excluded_samples.begin(), excluded_samples.end(), i)  !=   excluded_samples.end()) 
		{
			//these samples have been excluded from the info file
			//don't add it to the numeric indices, nor the nan indices - so the output isn't reinflated to include it
			infofile_excluded_indices.push_back(i);
		}
		else if ( isnan( inputdata.row(i).sum() )  )
		{
			//cout << "stripping NAN at " << i << endl;
			nan_indices.push_back(i);
		}
		else if (exclude_list.size() > 0  && find (exclude_list.begin(), exclude_list.end(), i)  !=   exclude_list.end())
		{
			//sample is excluded
			//cout << "mean intensity exclude " << i << endl;
			nan_indices.push_back(i);
		}
		else
		{
			num_indices.push_back(i);
		}
	}

	//cout << "num indices size " << num_indices.size() << endl;
	
    
	if (num_indices.size() == 0) {
        cout << "EMPTY DATA - EMPTY!!!!" << endl;
        //really shouldn't return this, probably better to throw an exception
		return inputdata;
	}


	MatrixXd num_data(num_indices.size(),inputdata.cols());
	for (int i =0; i < num_indices.size(); i++)
	{
		num_data.row(i) = inputdata.row(num_indices[i]);
	}

	//cout << num_data << endl;

	//cout << "numeric data" << endl;
	

	
	//I could now look for outliers - and then use num_indices[i] as the exclusion
	
	vector<int> outlier_indices;
    MatrixXd clean_data = num_data;
	
	if (!nonorm)
    {
        clean_data = strip_outliers(num_data, outlier_indices, outlier_range, set_outlier_range);
	}

	for (int i =0; i < outlier_indices.size(); i++)
	{
		nan_indices.push_back(num_indices[outlier_indices[i]]); 
		//cout << "stripping SNPwise outlier at " << inputdata.row(num_indices[outlier_indices[i]]) << endl;
	}
	
	

	sort (nan_indices.begin(), nan_indices.end(), lessthanfunction);

	
	cout << "EXCLUSION CHECK" << endl;
	for (int i =0; i < nan_indices.size(); i++)
	{
		cout << "i is: " << nan_indices[i] << " and data point is: " << inputdata.row(nan_indices[i]) << endl;
	}



	//Handling creating the correct gender data taking into account removals etc.
	vector<int> combinedexcluded_indices(nan_indices);
	combinedexcluded_indices.insert(combinedexcluded_indices.end(), infofile_excluded_indices.begin(), infofile_excluded_indices.end());
	sort (combinedexcluded_indices.begin(), combinedexcluded_indices.end(), lessthanfunction);
	
    //cout << combinedexcluded_indices.size() << " combined excluded" << endl;
	
	MatrixXi new_gender_data(clean_data.rows(),1);
	MatrixXi new_batch_data(clean_data.rows(),1);
    //cout << genders.rows() << " " << new_gender_data.rows();
	int counter = 0;
	for (int i = 0; i < inputdata.rows() ; i++)
	{
		if (!(combinedexcluded_indices.size() > 0  && find (combinedexcluded_indices.begin(), combinedexcluded_indices.end(), i)  !=   combinedexcluded_indices.end())) 
		{
			new_gender_data(counter,0) = genders(i,0);
            new_batch_data(counter,0) = batches(i,0);
			counter++;
		}

	}
	genders = new_gender_data;
    batches = new_batch_data;
	
	
	
	return clean_data;



}








MatrixXd probs_with_nans(const MatrixXd &post_probs,const vector<int> &nan_indices, vector<int> &excluded_samples, int original_size ) {


	//cout << "reinflating" << endl;
	//cout << "original size " << original_size << endl;
	//cout << post_probs.rows() << endl;

	//cout << "nan_indices size: " << nan_indices.size() << endl;
	
	
	
	MatrixXd post_probs_w_nans(original_size,post_probs.cols());

	vector<int> reinflatingindices = combineExclusionLists(nan_indices, excluded_samples);
	
	//cout << "reinflating indices size" << reinflatingindices.size() << endl;
	
	sort (reinflatingindices.begin(), reinflatingindices.end(), lessthanfunction);
	

	int num_probs_counter = 0;
	int index_counter = 0;
	for (int i = 0; i < original_size; i++)
	{
		if (index_counter < reinflatingindices.size() &&  i == reinflatingindices[index_counter]) {
			
			post_probs_w_nans.row(i) = MatrixXd::Zero(1, post_probs.cols());
			
			post_probs_w_nans(i,post_probs.cols()-1)=1;
			//move to the next nan index
			index_counter++;
		}
		else
		{
			post_probs_w_nans.row(i) = post_probs.row(num_probs_counter);
			num_probs_counter++;

		}
	}

	//cout << "successfully reinflated" << endl;
	
	//finally remove the excluded indices and return
	MatrixXd outputmatrix(post_probs_w_nans.rows()-excluded_samples.size(), post_probs_w_nans.cols());
	sort (excluded_samples.begin(), excluded_samples.end(), lessthanfunction);
	int excluded_sample_counter = 0;
	int outputcounter = 0;
	for (int i = 0; i < post_probs_w_nans.rows(); i++)
	{
		if ( excluded_sample_counter < excluded_samples.size() &&  i == excluded_samples[excluded_sample_counter]  )
		{
			//don't add
			excluded_sample_counter++;
		}
		else
		{
			//add
			outputmatrix.row(outputcounter) = post_probs_w_nans.row(i);
			outputcounter++;
		}
		
	}
	
	
	return outputmatrix;
	//return post_probs_w_nans;

}




//run in the case that all the values are NaN
MatrixXd probs_with_nans(int numclasses, vector<int> excluded_samples, int original_size ) {

	//cout << "original size - excluded " << original_size - excluded_samples.size() << endl;
	//cout << post_probs.rows() << endl;

	MatrixXd post_probs_w_nans(original_size - excluded_samples.size(),numclasses);

	//cout << post_probs_w_nans.rows() << " " << post_probs_w_nans.cols() << endl;

	for (int i = 0; i < original_size - excluded_samples.size(); i++)
	{
		post_probs_w_nans.row(i) = MatrixXd::Zero(1, numclasses);
		post_probs_w_nans(i,numclasses-1)=1;
	}

    //cout << "probs with NANs calculated" << endl;
    

	return post_probs_w_nans;
}





/********************* Return the pdf of a mv student's t distribution at x *****************/

//auxiliary functions to reduce matrix inverse calcs speed up some code


MatrixXd inverted_matrix(const MatrixXd &covar) {
	//speedup - for 2x2 matrices
	MatrixXd covar_inv_mat(2,2);
	covar_inv_mat(0,0)=covar(1,1);
	covar_inv_mat(1,1)=covar(0,0);
	covar_inv_mat(0,1)=-1*covar(0,1);
	covar_inv_mat(1,0)=-1*covar(1,0);
	covar_inv_mat = 1/( covar(0,0)*covar(1,1) - covar(1,0)*covar(0,1)  )*covar_inv_mat;
	
	return covar_inv_mat;
}

double det_of_mat(const MatrixXd &covar) {
	double covar_determ = covar(0,0)*covar(1,1)-covar(0,1)*covar(1,0);
	
	//if (covar.determinant() <= 0 )
	if (covar_determ <= 0 || isnan(covar_determ) || isinf(covar_determ) )
	{
		return 0;
	}
	return covar_determ;
}

double mvt_pdf_multiplier(const MatrixXd &covar, const double v, int dim) {
	
	double covar_determ = det_of_mat(covar); 
	double num = tgamma(dim*0.5*v)*sqrt(1.0/covar_determ);
	double den = sqrt(  pow(v*M_PI,dim) )*tgamma(v/2.0);
	return num/den;
}



class MvtStaticParts {

	
	public:
		MatrixXd inverse_covar;
		double numerator;
		MvtStaticParts(const MatrixXd &covar, const double v, int dim)
		{
			inverse_covar = inverted_matrix(covar);
			numerator = mvt_pdf_multiplier(covar, v, dim);
		}
	
};





//------------

double mahala_sq_dist(const MatrixXd &x_i, const MatrixXd &mu, const MvtStaticParts &staticcovar) {

	//speedup - for 2x2 matrices
	//MatrixXd covar_inv_mat(2,2);
	//covar_inv_mat(0,0)=covar(1,1);
	//covar_inv_mat(1,1)=covar(0,0);
	//covar_inv_mat(0,1)=-1*covar(0,1);
	//covar_inv_mat(1,0)=-1*covar(1,0);

	//covar_inv_mat = 1/( covar(0,0)*covar(1,1) - covar(1,0)*covar(0,1)  )*covar_inv_mat;

	//should be even faster!
	MatrixXd covar_inv_mat = staticcovar.inverse_covar;
	
	//MatrixXd test = ((x_i - mu)*covar.inverse()*(x_i - mu).transpose());
	MatrixXd test = ((x_i - mu)*covar_inv_mat*(x_i - mu).transpose());

	return test(0,0);
}


double mvt_pdf(const MatrixXd &x_i, const MatrixXd &mu, const MvtStaticParts &staticcovar, const double &v) {
    
	//double covar_determ = covar(0,0)*covar(1,1)-covar(0,1)*covar(1,0);

	//if (covar.determinant() <= 0 )
	//if (covar_determ <= 0 || isnan(covar_determ) || isinf(covar_determ) )
	//{
		//return 0;
	//}
	
	//dim = max(np.shape(x_i))
    //num = special.gamma(dim*0.5*v)*np.sqrt(1.0/np.linalg.det(covar))
    //den = np.sqrt(np.power(v*np.pi,dim))*special.gamma(v/2.0)*np.power(1+mahala_sq_dist(x_i,mu,covar),0.5*(v+dim))
    //pdf = num/den

	int dim = x_i.cols();

	//double num =  tgamma(dim*0.5*v)*sqrt(1.0/covar_determ);
	//double den = sqrt(  pow(v*M_PI,dim) )*tgamma(v/2.0)*pow(1.0+mahala_sq_dist(x_i,mu,covar),0.5*(v+dim))  ;

	double num = staticcovar.numerator;
	double den = pow(1.0+mahala_sq_dist(x_i,mu,staticcovar),0.5*(v+dim))  ;

	
	//cout << num << "  " << den << "  "  << endl;
	
	double output = num/den;

	if (isnan(output) || isinf(output) )
	{
		return 0;
	}

	return output;


}


/******************** Functions for making calls from posterior probabilities, and checking calls are equal************/

bool calls_equal(const vector<string> &call1, const vector<string> &call2) {
	
	if (call1.size() == 0 && call2.size() != 0) {
		return false;
	}
	
	if (call1.size() != 0 && call2.size() == 0) {
		return false;
	}
	
	bool equality = true;
	for (int i=0; i < call1.size(); i++)
	{
		equality = equality && (call1[i] == call2[i]);
	}
	return equality;
}

vector<string> calls_from_posterior_probs(MatrixXd post_probs) {
	vector <string> outputcalls;
	for (int i = 0; i < post_probs.rows(); i++) {
		
		
		double max_prob = post_probs.row(i).maxCoeff();
		
		if (max_prob < G_CALL_THRESHOLD ) {
			outputcalls.push_back("4");
		}
		else if (  isinf(post_probs.row(i).sum()) ||isnan(post_probs.row(i).sum())  )    
		{
			outputcalls.push_back("4");
		}
		else
		{
			if (post_probs(i,0) == max_prob) { outputcalls.push_back("1"); }
			if (post_probs(i,1) == max_prob) { outputcalls.push_back("3"); }
			if (post_probs(i,2) == max_prob) { outputcalls.push_back("2"); }
			if (post_probs(i,3) == max_prob) { outputcalls.push_back("4"); }
		}
	}
	return outputcalls;
}




/************************ Functions to remove samples before inferring mixture models, and reinsert them right after *******************/

MatrixXd strip_from_line_inference(const MatrixXd &inputdata, vector<int> &removed_indices, double outlier_range, MatrixXi &genders) {


    
    vector<int> num_indices;
	MatrixXd distances = inputdata.rowwise().norm();
	
	double mean_dist = distances.mean();
	double sd_dist = sd_of_data(distances)(0,0);
	
	
	for (int i = 0; i < distances.rows(); i++)
	{
		if ( distances(i,0) > mean_dist +  outlier_range )
		{
			removed_indices.push_back(i); 
			cout << "excluding point " << i << " with values:  " <<  inputdata.row(i) << " and distance: " << inputdata.row(i).norm() << " from inference." << endl;
		}
		else
		{
			num_indices.push_back(i);
		}
	}
	
	MatrixXd num_data(num_indices.size(),inputdata.cols());
    MatrixXi new_gender_data(num_indices.size(),genders.cols());
	for (int i =0; i < num_indices.size(); i++)
	{
		num_data.row(i) = inputdata.row(num_indices[i]);
        new_gender_data.row(i) = genders.row(num_indices[i]);
	}
    
    genders = new_gender_data;
	
	return num_data;
    
    
    

}






/******************** Run a mixture model of Student's t distributions and return the models for the block data*****/

MatrixXd Uijs(const MatrixXd &data, const MatrixXd &vs, const MatrixXd &mus, const vector<MatrixXd> &covars  ) {
	int dim = data.cols();
	MatrixXd mat_uijs(data.rows(), vs.rows());

	//speed improvement
	vector<MvtStaticParts> staticcovars;
	for (int c = 0; c < covars.size(); c++)
	{
		staticcovars.push_back(MvtStaticParts(covars[c],vs(c,0),dim));
	}
	

	for (int i = 0; i < data.rows(); i++) {
		for (int j = 0; j < vs.rows(); j++) {
			mat_uijs(i,j) = (vs(j,0)+dim)*1.0/(vs(j,0)+mahala_sq_dist(data.block(i,0,1,dim), mus.block(j,0,1,dim) , staticcovars[j]));
		}
	}

    return mat_uijs;

}


MatrixXd Tijs(const MatrixXd &data , const MatrixXd &js, const MatrixXd &vs, const MatrixXd &mus, const vector<MatrixXd> &covars ) {
	int dim = data.cols();
	MatrixXd mat_tijs(data.rows(), js.rows());

	//speed improvement
	vector<MvtStaticParts> staticcovars;
	for (int c = 0; c < covars.size(); c++)
	{
		staticcovars.push_back(MvtStaticParts(covars[c],vs(c,0),dim));
	}
	
	

	for (int i = 0; i < data.rows(); i++) {
		for (int j = 0; j < js.rows(); j++) {
			mat_tijs(i,j) = mvt_pdf(data.block(i,0,1,dim), mus.block(j,0,1,dim), staticcovars[j], vs(j,0))*js(j,0);
		}
		MatrixXd unnorm_block = mat_tijs.block(i,0,1,js.rows());
		mat_tijs.block(i,0,1,js.rows()) = unnorm_block*(1.0/unnorm_block.sum());
	}

	//cout << mat_tijs.rowwise().sum() << endl;
	return mat_tijs;
}




void mus_covs_for_Y_MT(MatrixXd &block_data, MatrixXd &starting_mus, vector<MatrixXd> &starting_covars, MatrixXd &starting_js, int dim) {
    

    //get the lowest x% of the data
        //next figure out the 
            //y% with the lowest y values
            //y% with the lowest x values 
    //use these to set up the 3 genotype classes
    
    MatrixXd distances = block_data.rowwise().norm();
    
    
    //cout << distances << endl;
    
    vector<int> sorteddistinds = argsort(distances,1, 0, false);
    
    /*
    for (int i = 0; i < sorteddistinds.size() ; i++)
    {
        cout << sorteddistinds[i] << endl;
    }
    cout << sorteddistinds.size() << endl;
    */
    
    MatrixXd sortedbydist  = sortedmatrix(block_data, 1, sorteddistinds);
    
    //cout << sortedbydist << endl;
    
    //split this matrix into bottom x% of rows - and rest
    int bottom_set = (int) (5*sortedbydist.rows()/100);
    
    MatrixXd bottom_data = sortedbydist.block(0,0,bottom_set,dim);
    MatrixXd rest_of_data = sortedbydist.block(bottom_set,0,sortedbydist.rows()-bottom_set,dim);
    
    
    //next - need to sort rest of data based on x and y values
    vector<int> sorted_low_y_inds = argsort(rest_of_data,1, 1, false);
    vector<int> sorted_low_x_inds = argsort(rest_of_data,1, 0, false);    
    
    int bottom_set_x = (int) (25*rest_of_data.rows()/100);
    int bottom_set_y = (int) (25*rest_of_data.rows()/100);
    
    MatrixXd x_homs = sortedmatrix(rest_of_data, 1, sorted_low_y_inds).block(0,0,bottom_set_x,dim);
    MatrixXd y_homs = sortedmatrix(rest_of_data, 1, sorted_low_x_inds).block(0,0,bottom_set_y,dim);    
    
    
    //then get means and covars - and done!
    
    MatrixXd mu_hom_x = x_homs.colwise().sum()/x_homs.rows();
    MatrixXd mu_hom_y = y_homs.colwise().sum()/y_homs.rows();
    MatrixXd mu_zeros = bottom_data.colwise().sum()/bottom_data.rows();
    
    cout << "Y chrom mus are" << endl;
    cout << mu_hom_x << " x_homs " << endl;
    cout << mu_hom_y << " y_homs " << endl;    
    cout << mu_zeros << " zeros " << endl;      
    
    
    //then get covariance matrices
    MatrixXd cov_hom_x =   (x_homs - mu_hom_x.replicate(x_homs.rows(), 1)).transpose() * (x_homs - mu_hom_x.replicate(x_homs.rows(), 1)) / x_homs.rows();
    MatrixXd cov_hom_y =   (y_homs - mu_hom_y.replicate(y_homs.rows(), 1)).transpose() * (y_homs - mu_hom_y.replicate(y_homs.rows(), 1)) / y_homs.rows();
    MatrixXd cov_zeros =   (bottom_data - mu_zeros.replicate(bottom_data.rows(), 1)).transpose() * (bottom_data - mu_zeros.replicate(bottom_data.rows(), 1)) / bottom_data.rows();
    
    cout << "Y chrom covars are" << endl;
    cout << cov_hom_x << " x_homs " << endl;
    cout << cov_hom_y << " y_homs " << endl;    
    cout << cov_zeros << " zeros " << endl;     
    
    
    starting_mus.row(0) = mu_hom_x;
    starting_mus.row(1) = mu_hom_y;
    starting_mus.row(2) << 0.0,0.0 ;
    
    starting_covars.push_back(cov_hom_x);
    starting_covars.push_back(cov_hom_y);
    starting_covars.push_back(cov_zeros);
    
    starting_js << 0.25, 0.25, 0.25, 0.25;
    
}



void mus_covs_for_autosomes_X(MatrixXd &int_data, MatrixXd &starting_mus, vector<MatrixXd> &starting_covars, MatrixXd &starting_js, int dim, int numclasses, int numcustomclasses) {

    int data_count = int_data.rows()*int_data.cols()/2;

	//declare the variables for KMeans++
	int attempts=150;
	double * points  = new double[data_count*dim];
	double * centres = new double[(numclasses-numcustomclasses)*dim];
	int * assignments = new int[data_count];
	double cost;
    
	for (int i = 0; i < int_data.rows(); i++)
	{
		for (int j = 0; j < int_data.cols(); j++)
		{
			//cout << "j is " << j << ' ' << dim << endl;
			points[i*dim+j] = int_data(i,j);
		}
	}
	cost = RunKMeansPlusPlus(data_count, numclasses-numcustomclasses, dim,points, attempts, centres, assignments);

	//setup the mus, covars, etc
	for (int i = 0; i < numclasses-numcustomclasses; i++ ) {
        
		MatrixXd cov;
		cov.setIdentity(dim, dim);
		starting_covars.push_back(cov*cost*2.0/data_count);
        
		for (int j = 0; j < dim; j++ ) {
			starting_mus(i,j) = centres[dim*i+j] ;
		}
		starting_js(i,0)=1.0/numclasses;
	}
    
	//TODO: delete the points used in the clustering - Done
	delete[] points;
	delete[] centres;
	delete[] assignments;
    
    
}




void genotype_mm_block_caller(const MatrixXd &block_data, const MatrixXd &vs, const vector<MatrixXd> &custommus, const vector<MatrixXd> &customcovars,
		MatrixXd &blockmus, vector<MatrixXd> &blockcovars, MatrixXd &blockjs, MatrixXd &blockvs, string chrom_type,  int numsteps = 30  ) {

	int dim = 2;
	int numclasses = 4;
	int numcustomclasses = 1;

	//This whole block data stuff is unneccesary - since our sampled block is the right size
	/*
	int data_count = block_data.rows()*block_data.cols()/2;
	//fix the data from a subblock
	MatrixXd int_data_T(dim,data_count);
	MatrixXd int_data;
	MatrixXd block_data_T(block_data.cols(),block_data.rows());
	block_data_T = block_data.transpose();
	int_data_T = Map<MatrixXd>(block_data_T.data(),dim,data_count);
	int_data = int_data_T.transpose();
	 */
	int data_count = block_data.rows()*block_data.cols()/2;
	MatrixXd int_data;
	int_data = block_data;

	//cout << int_data << endl;
	//cout << "int data" << endl;
	
	MatrixXd mus(numclasses,dim); 
	vector<MatrixXd> covars;
    MatrixXd js(numclasses,1); 
    
    
    
    if (chrom_type == "A" || chrom_type == "X")
    {
        mus_covs_for_autosomes_X(int_data, mus, covars, js,  dim,  numclasses,  numcustomclasses);
	}
    else
    {
        mus_covs_for_Y_MT(int_data, mus, covars, js, dim);
    }
    
    /**
	//declare the variables for KMeans++
	int attempts=150;
	double * points  = new double[data_count*dim];
	double * centres = new double[(numclasses-numcustomclasses)*dim];
	int * assignments = new int[data_count];
	double cost;

	for (int i = 0; i < int_data.rows(); i++)
	{
		for (int j = 0; j < int_data.cols(); j++)
		{
			//cout << "j is " << j << ' ' << dim << endl;
			points[i*dim+j] = int_data(i,j);
		}
	}
	cost = RunKMeansPlusPlus(data_count, numclasses-numcustomclasses, dim,points, attempts, centres, assignments);
    **/
    
    
    
    
    //COMMENTED BEFORE 28 MAY 2012
	//calculate the variance of the assigned pts
	//use as starting variance
	/*
	cout << cost*2/data_count << endl;
	for (int i = 0; i < numclasses-numcustomclasses; i++ ) {
		 cout << centres[dim*i] << ' ' << centres[dim*i+1] << endl;
	}
	*/




    /*
	//setup the mus, covars, etc
	for (int i = 0; i < numclasses-numcustomclasses; i++ ) {

		MatrixXd cov;
		cov.setIdentity(dim, dim);
		covars.push_back(cov*cost*2.0/data_count);

		for (int j = 0; j < dim; j++ ) {
			mus(i,j) = centres[dim*i+j] ;
		}
		js(i,0)=1.0/numclasses;
	}
    */
    
    
    
    
    
    
	//setup the custom mus and covars
	for (int i = 0; i < numcustomclasses; i++) {
		covars.push_back(customcovars[i]);
		mus.block(numclasses-numcustomclasses+i,0,1,dim) = custommus[i];
		js(numclasses-numcustomclasses+i,0)=1.0/numclasses;
	}

	//TODO: delete the points used in the clustering - Done
    /*
	delete[] points;
	delete[] centres;
	delete[] assignments;
    */
     
	//cout << mus << endl;
	//cout << js << endl;
	
	//HERE I should fix the means to what I think is best for X,Y chrom


	//then just run the algorithm & updates
	vector<string> current_calls;
	vector<string> old_calls;
	int globalsteps = 0;
	bool calls_matched = false;
	int stepcounter = 0;
	do {
		globalsteps++;
		if (calls_matched){
			stepcounter++;
		}
		
	
	//for (int step = 0; step < numsteps; step++) {
		//cout << "block step " << step << endl;
		MatrixXd mat_tijs = Tijs(int_data , js, vs, mus, covars );
		MatrixXd mat_uijs = Uijs(int_data , vs, mus, covars);

		//cout << "Tijs " << endl << mat_tijs << endl;
		//cout << "Uijs" << endl << mat_uijs << endl;


		js = (mat_tijs.colwise().sum().transpose())/(1.0*mat_tijs.rows());

		MatrixXd mat_tij_uij_product = mat_tijs.array() * mat_uijs.array();
		MatrixXd mus_sums = mat_tij_uij_product.transpose()*int_data;
		MatrixXd mus_normalizer(numclasses,dim);
		MatrixXd norm_factors = mat_tij_uij_product.colwise().sum().transpose().array().inverse();
		for (int i = 0; i < dim; i++) {
			mus_normalizer.block(0,i,numclasses,1) = norm_factors;
		}
		mus = mus_sums.array() * mus_normalizer.array();
        
        //set for the y, mt chroms
        if (!(chrom_type == "A" || chrom_type == "X"))
        {
            mus.row(2) << 0.0,0.0 ;
        }



		for (int i = 0; i < numclasses - numcustomclasses; i++) {
            

            
            
			MatrixXd data_minus_mu_j(data_count,dim);

			for (int p = 0; p < data_minus_mu_j.rows(); p++) {
				data_minus_mu_j.row(p)=int_data.row(p) - mus.row(i);
			}

			MatrixXd data_minus_mu_j_Tij = data_minus_mu_j;
			for (int p = 0; p < data_minus_mu_j_Tij.cols(); p++) {
				data_minus_mu_j_Tij.col(p)= data_minus_mu_j_Tij.col(p).array() * mat_tij_uij_product.col(i).array();
			}

			MatrixXd unnorm_covar_j = data_minus_mu_j_Tij.transpose() * data_minus_mu_j;
			double norm_fact = mat_tijs.colwise().sum()(0,i);
			
            
            //DO THE Y/MT correction here - make sure the Y chrom het -> unknown class doesn't go degenerate!
            if (!(chrom_type == "A" || chrom_type == "X")  && i == 2 &&   (isinf( (unnorm_covar_j/(1.0*norm_fact)).sum() )    || isnan(  (unnorm_covar_j/(1.0*norm_fact)).sum()  )    )  )
            {
                
            }
            else{
                covars[i]=unnorm_covar_j/(1.0*norm_fact);                
            }
            

			//cout << "Covar " << i << endl << covars[i] << endl;
            
            
            
            
		}

		//reset the means and covars of the custom classes
		for (int i = 0; i < numcustomclasses; i++) {
			mus.block(numclasses-numcustomclasses+i,0,1,dim) = custommus[i];
			covars[numclasses-numcustomclasses+i] = customcovars[i];
		}


        
        
        
		//cout << "Covar " << 3 << endl << covars[3] << endl;
		//cout << mus.block(numclasses-numcustomclasses,0,1,dim) << endl;

	//}
		
		old_calls = current_calls;
		current_calls = calls_from_posterior_probs(mat_tijs);     //probs_j_line(int_data,mus, covars,js, vs, mat_probs_j)) ; //mat_tijs);
		calls_matched = calls_equal(current_calls, old_calls);
		
	} while (  stepcounter < 3  && globalsteps <= numsteps );//&& !calls_equal(current_calls, old_calls)   );
	
	cout << "block steps run " << globalsteps << endl;


	//cout << "block mus" << endl;
	//cout << mus << endl;

	//cout << "block covars " << endl;
	
	//for (int i=0; i < covars.size(); i++)
	//{
	//	cout << covars[i] << endl;
	//}
	
	blockmus = mus;
	blockcovars = covars;
	blockjs = js;
	blockvs = vs;


	/*
	MatrixXd output_priors(data_count, numclasses);
	for (int i = 0; i < data_count; i++) {
		for (int j = 0; j < numclasses; j++ )
		{
			output_priors(i,j) = mvt_pdf(int_data.row(i), mus.row(j), covars[j], vs(j,0));  // *js(j,0);
		}
		output_priors.row(i) = output_priors.row(i)/(output_priors.row(i).rowwise().sum()(0,0));
		cout << " " << int_data.row(i) << " " << output_priors.row(i) << endl;

	}
	cout << js << endl;
	cout << endl;
	cout << endl;
	*/
}




/******************** Run a mixture model of Student's t distributions and return the probabilities for points*****/

/*
MatrixXd probs_j(const MatrixXd &data,const MatrixXi &blockmap,const vector<MatrixXd> &blockmus, const vector< vector<MatrixXd> > &blockcovars,
				const vector<MatrixXd> &blockjs, const vector<MatrixXd> &blockvs) {
	MatrixXd mat_probs_j(data.rows(), blockvs[0].rows());

	for (int i = 0; i < mat_probs_j.rows(); i++ ) {
		for (int j = 0; j < mat_probs_j.cols(); j++ )
		{
			mat_probs_j(i,j) = mvt_pdf(data.row(i), blockmus[blockmap(0,i)].row(j), blockcovars[blockmap(0,i)][j], blockvs[blockmap(0,i)](j,0))*blockjs[blockmap(0,i)](j,0);
		}
		mat_probs_j.row(i) = mat_probs_j.row(i)/(mat_probs_j.row(i).rowwise().sum()(0,0));
	}
	return mat_probs_j;

}
*/


MatrixXd probs_j(const MatrixXd &data,const MatrixXd &blockmus, const vector<MatrixXd>  &blockcovars,
				const MatrixXd &blockjs, const MatrixXd &blockvs) {
	MatrixXd mat_probs_j(data.rows(), blockvs.rows());

	//speed improvement
	int dim = data.cols();
	vector<MvtStaticParts> staticcovars;
	for (int c = 0; c < blockcovars.size(); c++)
	{
		staticcovars.push_back(MvtStaticParts(blockcovars[c],blockvs(c,0),dim));
	}
	
	
	for (int i = 0; i < mat_probs_j.rows(); i++ ) {
		for (int j = 0; j < mat_probs_j.cols(); j++ )
		{
			mat_probs_j(i,j) = mvt_pdf(data.row(i), blockmus.row(j), staticcovars[j], blockvs(j,0))*blockjs(j,0);
		}
		mat_probs_j.row(i) = mat_probs_j.row(i)/(mat_probs_j.row(i).rowwise().sum()(0,0));
	}
	return mat_probs_j;

}




MatrixXd probs_j_line(int dim, int numclasses,const MatrixXd &data,const MatrixXd &mus, const vector<MatrixXd>  &covars,
                      const MatrixXd &js, const MatrixXd &vs, const MatrixXd &blockmus, const vector<MatrixXd>  &blockcovars,
                      const MatrixXd &blockjs, const MatrixXd &blockvs) {
  
    //speed improvement
    vector<MvtStaticParts> staticcovars;
    for (int c = 0; c < covars.size(); c++)
    {
        staticcovars.push_back(MvtStaticParts(covars[c],vs(c,0),dim));
    }
 
 
 
    //calc mat_probs_j for all points, including the ones removed from inference
    MatrixXd mat_probs_j = probs_j(data,blockmus, blockcovars,blockjs,blockvs);
    //return the posterior calls - for all the points, including those removed from inference
    MatrixXd output_priors(data.rows(), numclasses);
    for (int i = 0; i < data.rows(); i++) {
        for (int j = 0; j < numclasses; j++ )
        {
            output_priors(i,j) = mvt_pdf(data.row(i), mus.row(j), staticcovars[j], vs(j,0))*mat_probs_j(i,j);
        }
        output_priors.row(i) = output_priors.row(i)/(output_priors.row(i).rowwise().sum()(0,0));
    }
    //cout << output_priors << endl;
    return output_priors;

 
 }

 

MatrixXd probs_j_rescue(const MatrixXd &data,const MatrixXd &blockmus, const vector<MatrixXd>  &blockcovars,
				const MatrixXd &blockjs, const MatrixXd &blockvs) {

	MatrixXd mat_probs_j(data.rows(), blockvs.rows());

	//speed improvement
	int dim = data.cols();
	vector<MvtStaticParts> staticcovars;
	for (int c = 0; c < blockcovars.size(); c++)
	{
		staticcovars.push_back(MvtStaticParts(blockcovars[c],blockvs(c,0),dim));
	}
	
	
	for (int i = 0; i < mat_probs_j.rows(); i++ ) {
		for (int j = 0; j < mat_probs_j.cols(); j++ )
		{
			mat_probs_j(i,j) = mvt_pdf(data.row(i), blockmus.row(j), staticcovars[j], blockvs(j,0))*blockjs(j,0);
			if (data(i,0)  >  blockmus(2,0) )
			{
				mat_probs_j(i,1) = 0;
			}
			else if (data(i,0)  <  blockmus(2,0))
			{
				mat_probs_j(i,0) = 0;
			}
		}
		mat_probs_j.row(i) = mat_probs_j.row(i)/(mat_probs_j.row(i).rowwise().sum()(0,0));
	}
	return mat_probs_j;

}





/* Mixture Model Likelihood calculation function - takes in the mixture details, and outputs the likelihood value  */



double mm_likelihood(const MatrixXd &data,const MatrixXd &mus, const vector<MatrixXd>  &covars,
				const MatrixXd &js, const MatrixXd &vs) {
	MatrixXd mat_probs_j(data.rows(), vs.rows());

	//speed improvement
	int dim = data.cols();
	vector<MvtStaticParts> staticcovars;
	for (int c = 0; c < covars.size(); c++)
	{
		staticcovars.push_back(MvtStaticParts(covars[c],vs(c,0),dim));
	}
	
	for (int i = 0; i < mat_probs_j.rows(); i++ ) {
		for (int j = 0; j < mat_probs_j.cols(); j++ )
		{
			mat_probs_j(i,j) = mvt_pdf(data.row(i), mus.row(j), staticcovars[j], vs(j,0))*js(j,0);
			if (isnan(mat_probs_j(i,j)) || isinf(mat_probs_j(i,j)) ) {
				cout << "row " << i << " " << data.row(i) << " ---- " << mat_probs_j.row(i) << endl;
				cout << mus << endl;
				cout << js << endl;
				cout << covars[j].determinant() << endl;
			}
		}
	}


	MatrixXd log_mat = mat_probs_j.rowwise().sum().unaryExpr(ptr_fun(logcoeff));

	/*
	cout << "likelihood calculation"<<endl;
	cout << mat_probs_j.rowwise().sum() << endl;
	cout << log_mat << endl;
	*/
	return log_mat.sum();

}





/******************** Functions for ouutputting the calls and posterior probabilities obtained from the caller *****/


MatrixXd postprobstounknown(MatrixXd &post_probs)
{
	MatrixXd post_all_unknown = MatrixXd::Zero(post_probs.rows(),post_probs.cols());
	post_all_unknown.col(3).setConstant(1);
	return post_all_unknown;
}

void outputtitleinfo(ofstream &outputcallsfile, ofstream &outputprobsfile, vector<string> individuals)
{

	outputcallsfile << "SNP Coor Alleles Pert";
	outputprobsfile << "SNP Coor Alleles Pert";
	
	for (int i = 0; i < individuals.size(); i++)
	{
		outputcallsfile << " " << individuals[i];
		outputprobsfile << " " << individuals[i];
	}
	
	outputcallsfile << "\n";
	outputprobsfile << "\n";
}

void outputpostprobs(MatrixXd &post_probs, ofstream &outputprobsfile, vector <string> &snpdesc, string chrom_type)
{
	outputprobsfile  << snpdesc[0] << " " << snpdesc[1] << " " << snpdesc[2];
	for (int i = 0; i < post_probs.rows(); i++) {
		
		if (  isinf(post_probs.row(i).sum()) ||isnan(post_probs.row(i).sum())  )    
		{
			outputprobsfile << " " << 0 << " " << 0  << " " << 0 << " " << 1;
		}
		
        if (!(chrom_type == "A" || chrom_type == "X"))
        {
            double combined_unknown = post_probs(i,2) + post_probs(i,3);
            outputprobsfile << " " << post_probs(i,0) << " " << post_probs(i,1)  << " " << 0.0 << " " << combined_unknown;            
        }
        else
        {
            outputprobsfile << " " << post_probs(i,0) << " " << post_probs(i,1)  << " " << post_probs(i,2) << " " << post_probs(i,3);
        }
        
		
	}
	outputprobsfile << "\n";
}



void outputcalls(MatrixXd &post_probs, ofstream &outputcallsfile, vector <string> &snpdesc, string chrom_type)
{
	outputcallsfile  << snpdesc[0] << " " << snpdesc[1] << " " << snpdesc[2] << " " << "0.9999";
	for (int i = 0; i < post_probs.rows(); i++) {

        MatrixXd rowdata = post_probs.row(i);
        
        if (!(chrom_type == "A" || chrom_type == "X"))
        {
            rowdata << post_probs(i,0),post_probs(i,1),0.0, post_probs(i,2)+post_probs(i,3);
        }


		//need to just output 1, 2, 3, 4 - depending
		//1 = AA, 2 = AB (heterozygote), 3 = BB, 4 = NN (no call)
		double max_prob = rowdata.maxCoeff();
		if (max_prob < G_CALL_THRESHOLD ) {
			outputcallsfile << " 4";
		}
		else if (  isinf(rowdata.sum()) ||isnan(rowdata.sum())  )    
		{
			outputcallsfile << " 4";
		}
		else
		{
			if (rowdata(0,0) == max_prob) { outputcallsfile << " 1"; }
			if (rowdata(0,1) == max_prob) { outputcallsfile << " 3"; }
			if (rowdata(0,2) == max_prob) { outputcallsfile << " 2"; }
			if (rowdata(0,3) == max_prob) { outputcallsfile << " 4"; }
		}
	}
	outputcallsfile << "\n";
}



/******************** functions for calculating HWE-p values ********/



MatrixXi countcalls(vector<string> calls) {
	MatrixXi callcount = MatrixXi::Zero(4, 1);
	for (int i = 0; i < calls.size(); i++)
	{
		if (calls[i]=="1"){ callcount(0,0)++; }
		if (calls[i]=="2"){ callcount(2,0)++; }
		if (calls[i]=="3"){ callcount(1,0)++; }
		if (calls[i]=="4"){ callcount(3,0)++; }
	}
	return callcount;

}


double hwepvalue_chisq(int na, int nb, int nab) {

	//cout << "running HWE chisq version " << endl;

	if (na + nb == 0) {
		return -1.0;  //numeric_limits<double>::quiet_NaN();
	}

	double p_a = (na*1.0)/(na+nb);
	double p_b = (nb*1.0)/(na+nb);

	double naa = (na - nab)/2;
	double nbb = (nb - nab)/2;

	int good_calls = nab + naa + nbb;

	double naa_h0 = good_calls * (p_a*p_a);
	double nbb_h0 = good_calls * (p_b*p_b);
	double nab_h0 = good_calls * (2*p_a*p_b);

	double test_stat = (naa - naa_h0)*(naa - naa_h0)/naa_h0 +  (nbb - nbb_h0)*(nbb - nbb_h0)/nbb_h0 + (nab - nab_h0)*(nab - nab_h0)/nab_h0 ;




	//return the chi-sq pvalue:

	if (isinf(test_stat))
	{
		cout << "test stat is infinitem :-(" << endl;
		//return 0;
		return numeric_limits<double>::quiet_NaN();
	}

	boost::math::chi_squared mydist(1);
	double p = boost::math::cdf(mydist,test_stat);
	return (1-p);

}








double hwepvalue(const vector <string> &calls, MatrixXi &batchinfo) {

    map<int, vector <string> > callmap;
    
    //Need to group calls into different batches. How to do this?
    
    
    int exclude_from_pcalc_count = 0;
    for (int p = 0; p < calls.size(); p++)
    {
        int batch = batchinfo(p,0);
        if (batch == -9)
        {
            exclude_from_pcalc_count++;
            continue;
        }
        if ( callmap.find(batch) == callmap.end() ) { 
            vector <string> newbatchcalls;
            callmap[batch] = newbatchcalls;
        }
        callmap[batch].push_back(calls[p]);
        
    }
    
    if (exclude_from_pcalc_count == calls.size())
    {
        //all the calls are excluded from pvalue calculations
        return 1.0;
    }
    
    //then for each key
    vector <double> batchpvals;
    vector <int> batchsizes;
    
    for(std::map< int, vector <string> >::iterator iter = callmap.begin(); iter != callmap.end(); ++iter)
    {
        int key =  iter->first;
        vector<string> batchcalls = callmap[key];
        double batch_hwep;
    
        int na = 0;
        int nb = 0;
        int nab = 0;

        //cout << "HWE-p check" << endl;

        for (int ind = 0; ind < batchcalls.size(); ind++)
        {
            if (batchcalls[ind].compare("1") == 0) {
                na=na+2;
            }
            if (batchcalls[ind].compare("2") == 0) {
                nab++;
                na++;
                nb++;
            }
            if (batchcalls[ind].compare("3") == 0) {
                nb=nb+2;
            }
        }

        //makes sure na is the minor allele to speed up computation
        if (na > nb )
        {
            int tmp = nb;
            nb = na;
            na = tmp;
        }
	//cout << "batch " << key << endl;
	//cout << "na " << na << endl;
	//cout << "nab " << nab << endl;
	//cout << "nb " << nb << endl;

        int N = (na-nab)/2 + (nb-nab)/2 + nab;
        
        batchsizes.push_back(N);
        
        if (N == 0) {/*return*/ batch_hwep = -1.0;}  //numeric_limits<double>::quiet_NaN()

        double naa_exp = ( ((na*1.0)/(na+nb))*((na*1.0)/(na+nb))*N );
        double nbb_exp = ( ((nb*1.0)/(na+nb))*((nb*1.0)/(na+nb))*N );
        double nab_exp = ( 2*((nb*1.0)/(na+nb))*((na*1.0)/(na+nb))*N );




        //no need to run an exact test - chi-squared is good enough
        /*
        cout << "na is" << na << endl;
        cout << "nb is" << nb << endl;
        cout << "naa_exp is" << naa_exp << endl;
        cout << "nbb_exp is" << nbb_exp << endl;
        cout << "nab_exp is" << nab_exp << endl;
        */

        if (na > 100 && nb > 100 && naa_exp >50 && nbb_exp >50 && nab_exp > 50  )
        {
            double hwep_chisq = hwepvalue_chisq(na, nb, nab);
            //cout << "HWE by chisq is " << hwep_chisq << endl;
            /*return*/ batch_hwep = hwep_chisq;
        }


        MatrixXd probs= MatrixXd::Zero(1, na+1);

        //cout << "actual nab results" << endl;
        //cout << (na-nab)/2 <<  " " << (nb-nab)/2 << " " << nab << endl;
        //cout << "========" << endl;


        int counter_up = nab;
        int counter_down = nab;
        probs(0,nab)=1;

        while (counter_up <= na-2)
        {
            counter_up = counter_up + 2;
            int naa_c = (na-counter_up)/2;
            int nbb_c = N-counter_up-naa_c; //(nb-counter_up)/2;
            //cout << naa_c << " " << nbb_c << " " << counter_up << endl;
            if (nbb_c >= 0 && naa_c >= 0) {
                probs(0,counter_up)=probs(0,counter_up-2)*4*naa_c*nbb_c*1.0/((counter_up+2)*(counter_up+1));
            }
        }

        while (counter_down >= 0+2)
        {
            counter_down = counter_down - 2;
            int naa_c = (na-counter_down)/2;
            int nbb_c = N-counter_down-naa_c;//(nb-counter_down)/2;
            //cout << naa_c << " " << nbb_c << " " << counter_down << endl;
            if (nbb_c >= 0 && naa_c >= 0) {
                probs(0,counter_down)=probs(0,counter_down+2)*counter_down*(counter_down-1)*1.0/(4*(naa_c+1)*(nbb_c+1));
            }
        }

        if (probs.sum() == 0){/*return*/ batch_hwep = -1.0;}

        //got my probs now sum them up

        double sum_probs_extreme = 0;
        for (int j = 0; j < probs.cols(); j++)
        {
            if (probs(0,j) <= probs(0,nab))
            {
                sum_probs_extreme = sum_probs_extreme + probs(0,j);
            }
        }

        //cout << probs << endl;
        //cout << sum_probs_extreme << endl;
        //cout << probs.sum() << endl;
        /*return*/ batch_hwep = sum_probs_extreme*1.0/probs.sum();

        
        batchpvals.push_back(batch_hwep);
        
    }
    //cout << "    ++++hwe p-vals by batch++++" << endl;
    //cout << "        batch sizes: "
    //for (int i=0; i<batchsizes.size();i++){cout << batchsizes[i] << " ";}
    //cout << endl;
    //cout << "        batch p-vals: "
    //for (int i=0; i<batchpvals.size();i++){cout << batchpvals[i] << " ";}
    //cout << endl;
    //cout << "    +++++++++" << endl;
    
    //finally do some stuffs with the p values
    boost::math::normal ndist;
    if (batchpvals.size() == 1)
    {
        //cout << "batch size is 1" << endl;
        return batchpvals[0];
    }
    else if (batchpvals.size() > 1)
    {
        double wsum_zs;
        double sum_ws;
        //need to weighted combine
        bool all_missing = true;
        
        for (int pind = 0; pind < batchpvals.size(); pind++)
        {
            sum_ws = sum_ws + batchsizes[pind]*batchsizes[pind];
        }
        double sqrt_sum_ws = sqrt(sum_ws);
        
        for (int pind = 0; pind < batchpvals.size(); pind++)
        {
            if (!(batchpvals[pind] < 0))
            {
                all_missing = false;
                double pval = batchpvals[pind];
                //to stop the quantile function crapping out
                if (pval > 0.99999999) { pval = 0.99999999; }
                
                //cout << "min limit zscore " << min_limit_zscore << endl;
                //cout << "p1 is " << p1 << endl;

                double zscore;  
                try
                {
                    //cout << " pval is " << pval << endl;
                    zscore = boost::math::quantile(ndist, ((double)1.0)-pval/((double)2.0));
	            //cout << " zscore boost is " << zscore << endl;
                }
                catch (std::exception& e)
                {
                    //cout << e.what() << endl;
                    //cout << "before going into approx_quantile  " << pval << " " << ((double)1.0)-pval/((double)2.0) << endl;
                    zscore = approx_quantile(((double)1.0)-pval/((double)2.0));
                    if (isinf(zscore)) {
                        zscore = numeric_limits<double>::max( );
                    }
                    //cout << "Z score book approx is  " << zscore << endl;
                }
                int weight = batchsizes[pind];
                wsum_zs = wsum_zs + (weight/sqrt_sum_ws)*zscore;
                //cout << "adding from batch " << pind << " weight " << weight << endl;
            }
        }
        if (all_missing)
        {
            return -1.0;
        }
        else
        {
            double test_stat = wsum_zs; ///sqrt(sum_ws);
            if (isinf(test_stat) || isnan(test_stat))
            {
                //cout << "batch test stat is nan/inf :-(" << endl;
                //return 0;
                return numeric_limits<double>::quiet_NaN();
            }
            //cout << "test stat is " << test_stat << endl;
            //return 1-approx_right_tail_cdf(-1*test_stat);
            double return_pval =  ((double)2.0)*approx_right_tail_cdf(test_stat);
            if (isinf(return_pval) || isnan(return_pval))
            {
                return numeric_limits<double>::quiet_NaN();
            }
            return return_pval;
            //return  1-boost::math::cdf(ndist,test_stat) ;
        }
    }
    return -1.0;
}








/************************ Mixture Model by SNP functions ******************************/





MatrixXd Tijs_line(const MatrixXd &data, const MatrixXd &probs_j , const MatrixXd &js, const MatrixXd &vs, const MatrixXd &mus, const vector<MatrixXd> &covars, string chrom_type , MatrixXi snp_genders) {
	int dim = data.cols();
	MatrixXd mat_tijs(data.rows(), js.rows());

	//speed improvement
	vector<MvtStaticParts> staticcovars;
	for (int c = 0; c < covars.size(); c++)
	{
		staticcovars.push_back(MvtStaticParts(covars[c],vs(c,0),dim));
	}

	for (int i = 0; i < data.rows(); i++) {
		
        //gender specific fixes
        if (chrom_type == "Y" && snp_genders(i,0) == 2)
        {
            mat_tijs.block(i,0,1,js.rows()) << 0.0, 0.0, 1.0, 0.0;
            continue;
        }
        
        
        //for each point check the max probs j,  then decide the scaling
		double p_proportion = 1.0;
		if (probs_j.row(i).maxCoeff() < 0.90) {
			p_proportion = 0.0;
		}
        if (!(chrom_type == "A" || chrom_type == "X") && probs_j.row(i).maxCoeff() < 0.95)
        {
            p_proportion = 0.0;
        }
		for (int j = 0; j < js.rows(); j++) {
			mat_tijs(i,j) = mvt_pdf(data.block(i,0,1,dim), mus.block(j,0,1,dim), staticcovars[j], vs(j,0))*((1-p_proportion)*js(j,0)+p_proportion*probs_j(i,j));
		}
        
        //gender specific fixes - THIS DIDN'T GIVE GOOD RESULTS, so commented out
        //if (chrom_type == "X" && snp_genders(i,0) == 1)
        //{
        //    mat_tijs(i,2) = 0.0;
        //}
        
		MatrixXd unnorm_block = mat_tijs.block(i,0,1,js.rows());
		mat_tijs.block(i,0,1,js.rows()) = unnorm_block*(1.0/unnorm_block.sum());
	}

	//cout << mat_tijs.rowwise().sum() << endl;
	return mat_tijs;
}


/*
MatrixXd pjs_by_hwe(MatrixXd Tijs) {

	MatrixXd pjs(4,1);
	MatrixXd tijs_sum = mat_tijs.colwise().sum().transpose();


	double pj = 2*(tijs_sum(0)+tijs_sum(2))/(2*tijs_sum(0) + 2*tijs_sum(1) + 3*tijs_sum(2));
	double w = (tijs_sum(3))/(tijs_sum(0)+tijs_sum(1)+tijs_sum(2));

	pjs(0,0) = pj*pj;
	pjs(1,0) = (1-pj)*(1-pj);
	pjs(2,0) = 2*pj*(1-pj);
	pjs(3)


}
*/


void genotype_mm_line_caller_w_priors(const MatrixXd &line_data, const MatrixXd &vs, const vector<MatrixXd> &custommus, const vector<MatrixXd> &customcovars,
		const MatrixXd &blockmus, const vector<MatrixXd> &blockcovars, const MatrixXd &blockjs, const MatrixXd &blockvs,const MatrixXd &block_sd,
        MatrixXd &linemus,  vector<MatrixXd> &linecovars,  MatrixXd &linejs,  MatrixXd &linevs,string chrom_type, MatrixXi snp_genders,  int numsteps = 14  ) {

	
	int dim = 2;
	int numclasses = 4;
	int numcustomclasses = 1;

	
    
    MatrixXd int_data = line_data;
	
	int data_count = int_data.rows();
    

    
    //int_data = transform_data(int_data);

	MatrixXd sd_int_data = sd_of_data(int_data);
	MatrixXd scaling_sd = (sd_int_data.array() / block_sd.array());
	MatrixXd scaling_matrix = scaling_sd.transpose() * scaling_sd;


	
	MatrixXd mus(numclasses,dim);
	vector<MatrixXd> covars;
    MatrixXd js(numclasses,1);

    MatrixXd alphas(numclasses,dim);
	vector<MatrixXd> wish_covars;

	MatrixXd wish_deg_fr = MatrixXd::Constant(numclasses, 1, 100);
	MatrixXd betas = MatrixXd::Constant(numclasses, 1, 1);

	

    mus =  MatrixXd::Zero(numclasses, dim);
    js = MatrixXd::Zero(numclasses, 1);
	for (int j = 0; j < numclasses-numcustomclasses; j++ ) {
		covars.push_back(MatrixXd::Zero(dim,dim));
		wish_covars.push_back(MatrixXd::Zero(dim,dim));
	}


	mus = blockmus;
	js = blockjs;

    /*
	cout << "blockjs are" << endl;
	cout << blockjs << endl;
	cout << "blockmus are" << endl;
	cout << blockmus << endl;
    */

	for (int j = 0; j < numclasses-numcustomclasses; j++ ) {
		covars[j] = covars[j].array() + blockcovars[j].array()*scaling_matrix.array();
        wish_covars[j] = wish_covars[j].array() + blockcovars[j].array();              

	}


	//Scaling - the mus
	MatrixXd mu_scaling_factors = int_data.colwise().sum()/int_data.rows();
    
    MatrixXd scaler_divisor = mus.row(2);
    if (!(chrom_type == "A" || chrom_type == "X") )
    {
        scaler_divisor << mus(0,0), mus(1,1);
    }
	double mu_scale = (mu_scaling_factors.array()/scaler_divisor.array()).maxCoeff();

	mus = mus * mu_scale;
	alphas = mus;
	//End scaling

	//setup the custom mus and covars
	for (int i = 0; i < numcustomclasses; i++) {
		covars.push_back(customcovars[i]);
		mus.block(numclasses-numcustomclasses+i,0,1,dim) = custommus[i];
	}




	//then just run the algorithm & updates

	MatrixXd mat_probs_j = probs_j(int_data,blockmus, blockcovars,blockjs,blockvs);

	/*
	cout << mus << endl;
	cout << covars[0] << endl << covars[1] << endl << covars[2] << endl << covars[3] << endl;
	cout << js << endl;
	cout << alphas << endl;
	cout << wish_covars[0] << endl << wish_covars[1] << endl << wish_covars[2] <<  endl;
	cout << wish_deg_fr << endl;
	cout << betas << endl;
	cout << endl ;
    */
	//cout << mat_probs_j;
	


	/*

	vector<string> current_calls = calls_from_posterior_probs(probs_j(int_data,mus, covars, js, vs));
	vector<string> old_calls;

	int stepcounter = 0;
	do {
		stepcounter++;


			MatrixXd post_probs = probs_j(int_data,mus, covars, js, vs);
			old_calls = current_calls;
			current_calls = calls_from_posterior_probs(post_probs);

			//cout << "got current calls, now comparing to old calls in rescue" << endl;


		} while (!calls_equal(current_calls, old_calls));


	*/


	vector<string> current_calls;
	vector<string> old_calls;
	int globalsteps = 0;
	bool calls_matched = false;
	int stepcounter = 0;
	do {
		globalsteps++;
		if (calls_matched){
			stepcounter++;
		}


	//for (int step = 0; step < numsteps; step++) {
		//cout << "line mode step " << step << endl;
		MatrixXd mat_tijs = Tijs_line(int_data, mat_probs_j , js, vs, mus, covars ,chrom_type, snp_genders);
		MatrixXd mat_uijs = Uijs(int_data , vs, mus, covars);

		//cout << mat_tijs << endl;
		//cout << " " << endl;
		//cout << mat_uijs << endl;

		js = (mat_tijs.colwise().sum().transpose())/(1.0*mat_tijs.rows());

		MatrixXd mat_tij_uij_product = mat_tijs.array() * mat_uijs.array();
		MatrixXd mus_sums = mat_tij_uij_product.transpose()*int_data;

		//cout << "alphas before" << endl;
		//cout << alphas << endl;

		MatrixXd mus_sums_alphas = alphas;


		MatrixXd mus_normalizer(numclasses,dim);
		MatrixXd norm_factors = mat_tij_uij_product.colwise().sum().transpose();
		for (int i = 0; i < dim; i++) {
			mus_normalizer.block(0,i,numclasses,1) = (norm_factors + betas).array().inverse();
			mus_sums_alphas.col(i) = mus_sums_alphas.col(i).array()*betas.array();
		}
		mus = (mus_sums+mus_sums_alphas).array() * mus_normalizer.array();
        
        //set for the y, mt chroms
        if (!(chrom_type == "A" || chrom_type == "X"))
        {
            mus.row(2) << 0.0,0.0 ;
        }
        

		
		//cout << mus_sums_alphas << endl;
		//cout << "alphas after" << endl;
		//cout << alphas << endl;
		//cout << "new mus" << endl;
		//cout << mus << endl;
        
		

		for (int i = 0; i < numclasses - numcustomclasses; i++) {
			MatrixXd data_minus_mu_j(data_count,dim);


			for (int p = 0; p < data_minus_mu_j.rows(); p++) {
				data_minus_mu_j.row(p)=int_data.row(p) - mus.row(i);
			}


			//data_minus_mu_j.rowwise() -= (VectorXd)mus.row(i);


			MatrixXd data_minus_mu_j_Tij = data_minus_mu_j;
			for (int p = 0; p < data_minus_mu_j_Tij.cols(); p++) {
				data_minus_mu_j_Tij.col(p)= data_minus_mu_j_Tij.col(p).array() * mat_tij_uij_product.col(i).array();
			}
			MatrixXd unnorm_covar_j = data_minus_mu_j_Tij.transpose() * data_minus_mu_j;
			//need to add the prior stuff here
			unnorm_covar_j = unnorm_covar_j + betas(i,0)*((mus.row(i)-alphas.row(i)).transpose()*(mus.row(i)-alphas.row(i))  ) + wish_covars[i];
			double norm_fact = mat_tijs.colwise().sum()(0,i) + 1 + (wish_deg_fr(i,0) - dim - 1);
			covars[i]=unnorm_covar_j/(1.0*norm_fact);
            
			//cout << "Covar " << i << endl << covars[i] << endl;
		}

		//reset the means and covars of the custom classes
		for (int i = 0; i < numcustomclasses; i++) {
			mus.block(numclasses-numcustomclasses+i,0,1,dim) = custommus[i];
			covars[numclasses-numcustomclasses+i] = customcovars[i];
		}


		old_calls = current_calls;
		current_calls = calls_from_posterior_probs(mat_tijs);     //probs_j_line(int_data,mus, covars,js, vs, mat_probs_j)) ; //mat_tijs);
		calls_matched = calls_equal(current_calls, old_calls);
	//}

	} while (  stepcounter < 3  && globalsteps <= numsteps );//&& !calls_equal(current_calls, old_calls)   );

	cout << "steps taken " << globalsteps << endl;

    
    
    linemus = mus; 
    linecovars = covars;
    linejs = js;
    linevs = vs;
    
    
	
	/*
	//speed improvement
	vector<MvtStaticParts> staticcovars;
	for (int c = 0; c < covars.size(); c++)
	{
		staticcovars.push_back(MvtStaticParts(covars[c],vs(c,0),dim));
	}
	
	

    //recalc mat_probs_j for all points, including the ones removed from inference
    mat_probs_j = probs_j(line_data,blockmus, blockcovars,blockjs,blockvs);
	//return the posterior calls - for all the points, including those removed from inference
	MatrixXd output_priors(line_data.rows(), numclasses);
	for (int i = 0; i < line_data.rows(); i++) {
		for (int j = 0; j < numclasses; j++ )
		{
			output_priors(i,j) = mvt_pdf(line_data.row(i), mus.row(j), staticcovars[j], vs(j,0))*mat_probs_j(i,j);
		}
		output_priors.row(i) = output_priors.row(i)/(output_priors.row(i).rowwise().sum()(0,0));
	}
	//cout << output_priors << endl;
	return output_priors;
    */

}












/******************** Run a mixture model of Student's t distributions, from different starting pts and return the probabilities for points*****/




MatrixXd filterMatrix(MatrixXd data, int number_excluded) {

	vector<int> filter_x;
	vector<int> filter_y;

	MatrixXd mod_data = untransform_data(data);;
	MatrixXd maxVals = mod_data.colwise().maxCoeff();

	//cout << "looking to exclude " << number_excluded << endl;

	for (int i =0; i < number_excluded; i++) {
		MatrixXd::Index minRow, minCol;
		mod_data.col(0).minCoeff(&minRow, &minCol);
		filter_x.push_back((int)minRow);
		//set to max - so this isn't found again
		mod_data((int)minRow,0) = maxVals(0,0)*10;

		mod_data.col(1).minCoeff(&minRow, &minCol);
		filter_y.push_back((int)minRow);
		//set to max so this index isn't found again
		mod_data((int)minRow,1) = maxVals(0,1)*10;

	}

	//cout << "got filters x and y " << filter_x.size() << " " << filter_y.size() << endl;

	//get a filter of values that are small on both axes
	vector<int> filter;
	for (int i =0; i < filter_x.size(); i++) {
		for (int j =0; j < filter_y.size(); j++) {
			if (filter_x[i] == filter_y[j]) {
				filter.push_back(filter_x[i]);
			}
		}
	}

	//cout << "filtering " << filter.size() << endl;



	MatrixXd filtered_data(data.rows() - filter.size(),data.cols());
	int positioncounter = 0;
	for (int i = 0; i < data.rows(); i++) {
		bool row_excluded = false;
		for (int j = 0; j< filter.size(); j++) {
			if (filter[j] == i) {
				row_excluded = true;
				//cout << "excluded row from data " << data.row(i)  << endl;
			}
		}
		if (!row_excluded) {
			filtered_data.row(positioncounter) = data.row(i);
			positioncounter++;
		}
	}

	//cout << "data filtered" << endl;

	return filtered_data;

}






void set_starting_pts(MatrixXd int_data, vector<MatrixXd> &startmus,  vector< vector<MatrixXd> > &startcovars,  vector<MatrixXd> &startjs,
		const vector<MatrixXd> &custommus, const vector<MatrixXd> &customcovars) {

	int dim = 2;
	int numclasses = 4;
	int numcustomclasses = 1;
	int data_count = int_data.rows();

	//declare the variables for KMeans++
	int attempts=5;
	double * points  = new double[data_count*dim];
	double * centres = new double[(numclasses-numcustomclasses)*dim];
	int * assignments = new int[data_count];
	double cost;

	for (int i = 0; i < int_data.rows(); i++)
	{
		for (int j = 0; j < int_data.cols(); j++)
		{
			//cout << "j is " << j << ' ' << dim << endl;
			points[i*dim+j] = int_data(i,j);
		}
	}
	cost = RunKMeansPlusPlus(data_count, numclasses-numcustomclasses, dim,points, attempts, centres, assignments);

	//cout << "kmeans was run" << endl;


	MatrixXd idcov;
	idcov.setIdentity(dim, dim);




    MatrixXd clean_data = int_data;
	if ((int)(0.01*int_data.rows()) > 0) {
		clean_data = filterMatrix(int_data,(int)(0.01*int_data.rows())  );
	}

	//cout << "filtered data " << endl;



    	//use data
	MatrixXd mus5(numclasses,dim);
	vector<MatrixXd> covars5;
    MatrixXd js5(numclasses,1);
    mus5.setConstant(-5);

    //set up the 2 real classes
    mus5(0,0)=clean_data.col(0).maxCoeff();
    mus5(0,1)=clean_data.col(1).mean();
    mus5(1,0)=clean_data.col(0).minCoeff();
    mus5(1,1)=clean_data.col(1).mean();
    mus5(2,0)=(clean_data.col(0).minCoeff()+clean_data.col(0).maxCoeff())/2;
    mus5(2,1)=clean_data.col(1).mean();
    //set up the covars - no biggie what they are for other classes - their class probs are set to zero
	for (int i = 0; i < numclasses - numcustomclasses; i++){
		covars5.push_back(idcov*cost*2.0/(data_count));
	}
    //finally set up the class probs
    js5 << 0.25, 0.25, 0.25, 0.25;

    //adding this setup
    startmus.push_back(mus5);
    startcovars.push_back(covars5);
    startjs.push_back(js5);





		//use data
	MatrixXd mus6(numclasses,dim);
	vector<MatrixXd> covars6;
	MatrixXd js6(numclasses,1);
	mus6.setConstant(-5);

	//set up the 2 real classes
	mus6(0,0)=clean_data.col(0).maxCoeff();
	mus6(0,1)=clean_data.col(1).mean();
	mus6(1,0)=clean_data.col(0).minCoeff();
	mus6(1,1)=clean_data.col(1).mean();
	mus6(2,0)=(clean_data.col(0).minCoeff()+clean_data.col(0).maxCoeff())*0.55;
	mus6(2,1)=clean_data.col(1).mean();
	//set up the covars - no biggie what they are for other classes - their class probs are set to zero
	for (int i = 0; i < numclasses - numcustomclasses; i++){
		covars6.push_back(idcov*cost*2.0/(data_count));
	}
	//finally set up the class probs
	js6 << 0.25, 0.25, 0.25, 0.25;

	//adding this setup
	startmus.push_back(mus6);
	startcovars.push_back(covars6);
	startjs.push_back(js6);






		//use data
	MatrixXd mus7(numclasses,dim);
	vector<MatrixXd> covars7;
	MatrixXd js7(numclasses,1);
	mus7.setConstant(-5);

	//set up the 2 real classes
	mus7(0,0)=clean_data.col(0).maxCoeff();
	mus7(0,1)=clean_data.col(1).mean();
	mus7(1,0)=clean_data.col(0).minCoeff();
	mus7(1,1)=clean_data.col(1).mean();
	mus7(2,0)=(clean_data.col(0).minCoeff()+clean_data.col(0).maxCoeff())*0.45;
	mus7(2,1)=clean_data.col(1).mean();
	//set up the covars - no biggie what they are for other classes - their class probs are set to zero
	for (int i = 0; i < numclasses - numcustomclasses; i++){
		covars7.push_back(idcov*cost*2.0/(data_count));
	}
	//finally set up the class probs
	js7 << 0.25, 0.25, 0.25, 0.25;

	//adding this setup
	startmus.push_back(mus7);
	startcovars.push_back(covars7);
	startjs.push_back(js7);



    

    //set the custom classes
    for (int i = 0; i < startmus.size(); i++) {

    	for (int j = 0; j < numcustomclasses; j++) {
    		startcovars[i].push_back(customcovars[j]);
    		startmus[i].block(numclasses-numcustomclasses+j,0,1,dim) = custommus[j];
    	}


    }




	//TODO: delete the points used in the clustering - Done
	delete[] points;
	delete[] centres;
	delete[] assignments;






}

void genotype_mm_rescue_caller(const MatrixXd &line_data, const MatrixXd &vs, const vector<MatrixXd> &custommus, const vector<MatrixXd> &customcovars,
		MatrixXd &blockmus,  vector<MatrixXd> &blockcovars,  MatrixXd &blockjs,  MatrixXd &blockvs, int numsteps = 100, int hwetestpoint = 15) {


	int dim = 2;
	int numclasses = 4;
	int numcustomclasses = 1;

	int data_count = line_data.rows()*line_data.cols()/2;
	MatrixXd int_data;
	int_data = line_data;


	//start off with a number of different starting points

	//run the method on a sequence of them
		//choose the one with the highest likelihood
		//run the method - maybe run with the split and merge too



	vector<MatrixXd> starting_pts_mus;
	vector<MatrixXd> starting_pts_js;
	vector< vector<MatrixXd> > starting_pts_covars;

	double curr_best_likelihood = -1*  numeric_limits<double>::max( );

	//cout << "min double is " << curr_best_likelihood << endl;

	MatrixXd best_mus(numclasses,dim);
	vector<MatrixXd> best_covars;
    MatrixXd best_js(numclasses,1);




	//TODO: set up the starting points
    //probably best to scale the priors here
    //SCALING THE PRIOR MUS AND SDS

    //cout << "going to scale data now" << endl;

    
    
    /**
	MatrixXd priormus_scaled(numclasses,dim);
	vector<MatrixXd> priorcovars_scaled;

    MatrixXd sd_int_data = sd_of_data(int_data);
	MatrixXd scaling_sd = (sd_int_data.array() / prior_sd.array());
	MatrixXd scaling_matrix = scaling_sd.transpose() * scaling_sd;
     **/
	
	//cout << "Scaling matrix " << endl;
	//cout << scaling_matrix << endl;
	
    /**
	for (int j = 0; j < numclasses-numcustomclasses; j++ ) {
		priorcovars_scaled.push_back( priorcovars[j].array()*scaling_matrix.array() );
	}
	//Scaling - the mus
	MatrixXd mu_scaling_factors = int_data.colwise().sum()/int_data.rows();
	double mu_scale = (mu_scaling_factors.array()/priormus.row(2).array()).maxCoeff();
	priormus_scaled = priormus * mu_scale;
	//End scaling

	//setup the custom mus and covars
	for (int i = 0; i < numcustomclasses; i++) {
		priorcovars_scaled.push_back(customcovars[i]);
		priormus_scaled.block(numclasses-numcustomclasses+i,0,1,dim) = custommus[i];
	}
     **/


	//get the starting points
	//cout << "setting starting points" << endl;



    set_starting_pts(int_data,starting_pts_mus,  starting_pts_covars,  starting_pts_js,
    		custommus, customcovars);

    //cout << "number of starting pts" << endl;
    //cout << starting_pts_mus.size() << endl;



	for (int starting_pt = 0; starting_pt < starting_pts_mus.size(); starting_pt++) {

		//run the EM to get the best clustering here

		MatrixXd mus(numclasses,dim);
		vector<MatrixXd> covars;
	    MatrixXd js(numclasses,1);


	    //cout << "starting pt " << starting_pt << endl;

	    /*
	    cout << "starting mus" << endl;
	    cout << starting_pts_mus[starting_pt] << endl;
	    cout << "starting js" << endl;
	    cout << starting_pts_js[starting_pt] << endl;
		*/

		mus = starting_pts_mus[starting_pt];
		covars = starting_pts_covars[starting_pt];
		js = starting_pts_js[starting_pt];

		vector<string> current_calls; //= calls_from_posterior_probs(probs_j(int_data,mus, covars, js, vs));
		vector<string> old_calls;

		int stepcounter = 0;
		bool hwep_fail = false;
		do {
			stepcounter++;
			//cout << "rescue step " << stepcounter << endl;



			MatrixXd mat_tijs = Tijs(int_data , js, vs, mus, covars );
			MatrixXd mat_uijs = Uijs(int_data , vs, mus, covars);

			//cout << "Tijs " << endl << mat_tijs << endl;
			//cout << "Uijs" << endl << mat_uijs << endl;

			js = (mat_tijs.colwise().sum().transpose())/(1.0*mat_tijs.rows());


			//cout << "js in rescue caller" << endl;
			//cout << js << endl;


			//MatrixXd noupdate = MatrixXi::Zero(1, sample.rows());

			MatrixXd mat_tij_uij_product = mat_tijs.array() * mat_uijs.array();
			MatrixXd mus_sums = mat_tij_uij_product.transpose()*int_data;
			MatrixXd mus_normalizer(numclasses,dim);
			MatrixXd norm_factors = mat_tij_uij_product.colwise().sum().transpose().array().inverse();




			for (int i = 0; i < dim; i++) {
				mus_normalizer.block(0,i,numclasses,1) = norm_factors;
			}


			/*
			cout << "mus sums in rescue caller" << endl;
			cout << mus_sums << endl;
			cout << "norm factors in rescue caller" << endl;
			cout << mus_normalizer << endl;
			*/

			//fix for Nan/Inf issues when dealing with zero probability classes
			MatrixXd mus_backup = mus;
			MatrixXd mus_prospective = mus_sums.array() * mus_normalizer.array();
			mus = mus_sums.array() * mus_normalizer.array();

			for (int i = 0; i < numclasses - numcustomclasses; i++)
			{
				if (isinf(mus.row(i).sum()) ||isnan(mus.row(i).sum())) {
					mus.row(i) = mus_backup.row(i);
				}
			}


			//cout << "and the mus for this step are..." << endl;
			//cout << mus << endl;


			//cout << "rescue js" << endl; 
			//cout << js << endl;
			//cout << norm_factors << endl;
			//cout << "rescue mus" << endl;
			//cout << mus << endl;

			for (int i = 0; i < numclasses - numcustomclasses; i++) {
				//fix for Nan/Inf issues when dealing with zero probability classes
				if (isinf(mus_prospective.row(i).sum()) ||isnan(mus_prospective.row(i).sum())) {
					continue;
				}

				MatrixXd data_minus_mu_j(data_count,dim);

				for (int p = 0; p < data_minus_mu_j.rows(); p++) {
					data_minus_mu_j.row(p)=int_data.row(p) - mus.row(i);
				}

				MatrixXd data_minus_mu_j_Tij = data_minus_mu_j;
				for (int p = 0; p < data_minus_mu_j_Tij.cols(); p++) {
					data_minus_mu_j_Tij.col(p)= data_minus_mu_j_Tij.col(p).array() * mat_tij_uij_product.col(i).array();
				}

				MatrixXd unnorm_covar_j = data_minus_mu_j_Tij.transpose() * data_minus_mu_j;
				double norm_fact = mat_tijs.colwise().sum()(0,i);
				covars[i]=unnorm_covar_j/(1.0*norm_fact);
				//cout << "Covar " << i << endl << covars[i] << endl;
			}

			//reset the means and covars of the custom classes
			for (int i = 0; i < numcustomclasses; i++) {
				mus.block(numclasses-numcustomclasses+i,0,1,dim) = custommus[i];
				covars[numclasses-numcustomclasses+i] = customcovars[i];
			}

			//cout << "Covar " << 3 << endl << covars[3] << endl;
			//cout << mus.block(numclasses-numcustomclasses,0,1,dim) << endl;
			//cout << "calculating post_probs" << endl;

			MatrixXd post_probs = probs_j(int_data,mus, covars, js, vs);
			old_calls = current_calls;
			current_calls = calls_from_posterior_probs(post_probs);

			//cout << "got current calls, now comparing to old calls in rescue" << endl;

			/*
			if (stepcounter == hwetestpoint  )
			{
				double hwep_test = hwepvalue(current_calls);
				hwep_fail = isnan(hwep_test) || hwep_test < 5*1e-15;
			}
			*/

		} while ( !calls_equal(current_calls, old_calls)  ); //&&  ( stepcounter <= numsteps   )     );   // && !hwep_fail );

		cout << "rescue steps taken " << stepcounter << endl;
		
		//|| hwepvalue(current_calls)  ||  isnan(hwep) || hwep < 5*1e-15


		//cout << "fall out of rescue loop" << endl;
		//calculate the likelihood from this
		//double hwep = hwepvalue(current_calls);
		double likelihood = mm_likelihood(int_data,mus, covars,	js, vs);

		//cout << numeric_limits<double>::min( ) << endl;
		//cout << "likelihood is " << likelihood << endl;
		//cout << "current best likelihood is " << curr_best_likelihood << endl;

		if (likelihood > curr_best_likelihood)
		{
			best_mus = mus;
			best_covars = covars;
		    best_js = js;
		    curr_best_likelihood = likelihood;

		}




	}


	/*
	cout << "rescued mus" << endl;
	cout << best_mus << endl;

	cout << "rescued js" << endl;
	cout << best_js << endl;
	*/

	blockmus = best_mus;
	blockcovars = best_covars;
	blockjs = best_js;
	blockvs = vs;





}



MatrixXi arrange_mixture_components_1class(const MatrixXi &callcount,const MatrixXd &data,  MatrixXd &mus,  vector<MatrixXd>  &covars,  MatrixXd &js,  MatrixXd &vs)
{
	MatrixXi argsorts(4,1);
	if (callcount(0,0)==0 && callcount(1,0)==0 && callcount(2,0)!=0)
	{
		if(mus(2,0) > 0)
		{
			//x>y - set mus[2] as allele x, rest have no points, so don't matter
			argsorts << 2,1,0,3;
		}
		else
		{
			argsorts << 0,2,1,3;
		}
	}
	if (callcount(1,0)==0 && callcount(2,0)==0 && callcount(0,0)!=0)
	{
		if(mus(0,0) > 0)
		{
			//x>y - set mus[0] as allele x, rest have no points, so don't matter
			argsorts << 0,1,2,3;
		}
		else
		{
			argsorts << 2,0,1,3;
		}
	}
	if (callcount(0,0)==0 && callcount(2,0)==0 && callcount(1,0)!=0)
	{
		if(mus(1,0) > 0)
		{
			//x>y - set mus[0] as allele x, rest have no points, so don't matter
			argsorts << 1,0,2,3;
		}
		else
		{
			argsorts << 2,1,0,3;
		}
	}
	return argsorts;
}


MatrixXi arrange_mixture_components_2classes(const MatrixXi &callcount,const MatrixXd &data, MatrixXd &mus,  vector<MatrixXd> &covars,  MatrixXd &js,  MatrixXd &vs)
{
	MatrixXi argsorts(4,1);
	if (callcount(0,0)==0 && callcount(1,0)!=0 && callcount(2,0)!=0 ){
		if (callcount(1,0)>callcount(2,0))
		{
			//callcount[1] is the homozyg. class
			if(mus(1,0) > mus(2,0))
			{
				//x>y - set mus[0] as allele x, rest have no points, so don't matter
				argsorts << 1,0,2,3;
			}
			else
			{
				argsorts << 0,1,2,3;
			}
		}
		else
		{
			//callcount[2] is the homozyg. class
			if(mus(2,0) > mus(1,0))
			{
				//x>y - set mus[0] as allele x, rest have no points, so don't matter
				argsorts << 2,0,1,3;
			}
			else
			{
				argsorts << 0,2,1,3;
			}
		}
	}
	if (callcount(0,0)!=0 && callcount(1,0)==0 && callcount(2,0)!=0 ){
		if (callcount(0,0)>callcount(2,0))
		{
			//callcount[0] is the homozyg. class
			if(mus(0,0) > mus(2,0))
			{
				//x>y - set mus[0] as allele x, rest have no points, so don't matter
				argsorts << 0,1,2,3;
			}
			else
			{
				argsorts << 1,0,2,3;
			}
		}
		else
		{
			//callcount[2] is the homozyg. class
			if(mus(2,0) > mus(0,0))
			{
				//x>y - set mus[0] as allele x, rest have no points, so don't matter
				argsorts << 2,1,0,3;
			}
			else
			{
				argsorts << 1,2,0,3;
			}
		}
	}
	if (callcount(0,0)!=0 && callcount(1,0)!=0 && callcount(2,0)==0 ){
		if (callcount(0,0)>callcount(1,0))
		{
			//callcount[0] is the homozyg. class
			if(mus(0,0) > mus(1,0))
			{
				//x>y - set mus[0] as allele x, rest have no points, so don't matter
				argsorts << 0,2,1,3;
			}
			else
			{
				argsorts << 2,0,1,3;
			}
		}
		else
		{
			//callcount[1] is the homozyg. class
			if(mus(1,0) > mus(0,0))
			{
				//x>y - set mus[0] as allele x, rest have no points, so don't matter
				argsorts << 1,2,0,3;
			}
			else
			{
				argsorts << 2,1,0,3;
			}
		}
	}
	return argsorts;
}


void arrange_mixture_components(const MatrixXd &data, MatrixXd &mus,  vector<MatrixXd> &covars,  MatrixXd &js,  MatrixXd &vs, string chrom_type) {

	MatrixXd post_probs = probs_j(data,mus, covars, js, vs);
	vector<string> calls_from_probs = calls_from_posterior_probs(post_probs);
	MatrixXi callcount = countcalls(calls_from_probs);

	MatrixXd reord_mus(4,2);
	vector<MatrixXd> reord_covars(4);
    MatrixXd reord_js(4,1);
    MatrixXd reord_vs(4,1);

	//sanity check - is everything unknown?
	if (callcount(0,0)==0 && callcount(1,0)==0 && callcount(2,0)==0) {
		return;
	}

	MatrixXi argsorts(4,1);
    
    
    

	int num_zero_classes=0;
	for (int i = 0; i < callcount.rows()-1; i++) {
		if (callcount(i,0) == 0) {num_zero_classes++;}
	}

	//First check if 2 components are empty
	if (num_zero_classes == 2) {
		argsorts = arrange_mixture_components_1class(callcount,data,  mus,  covars,  js,  vs);
	}
	//second check if one component is empty
	if (num_zero_classes == 1) {
		argsorts = arrange_mixture_components_2classes(callcount,data, mus,  covars,  js,  vs);
	}

	if (callcount(0,0)!=0 && callcount(1,0)!=0 && callcount(2,0)!=0 ){
		//now we must have 3 clouds - closest to x-axis, closest to y axis and other one
		//get calls from mixture model & reorder
		MatrixXd maxvals = mus.block(0,0,3,2).colwise().maxCoeff();
		MatrixXd minvals = mus.block(0,0,3,2).colwise().minCoeff();
		/*
		cout << "minvals" << endl;
		cout << minvals << endl;
		*/

		argsorts(3,0) = 3;  //assign the custom class to the end

		for (int i = 0; i < 3; i++) {
			//pass
			if ( mus(i,0) == maxvals(0,0)) {
				argsorts(0,0) = i;
			}
			else if ( mus(i,0) == minvals(0,0)) {
				argsorts(1,0) = i;
			}
			else {
				argsorts(2,0) = i;
			}
		}




	}
    
    
    //set for the y, mt chroms
    if (!(chrom_type == "A" || chrom_type == "X"))
    {
        argsorts << 0,1,2,3 ;
    }
    
    

	//finally sort the clsses by argsort
    for (int i = 0; i < argsorts.rows(); i++) {
    	reord_mus.row(i) = mus.row(argsorts(i,0));
    	reord_js.row(i) = js.row(argsorts(i,0));
    	reord_vs.row(i) = vs.row(argsorts(i,0));
    	reord_covars[i] = covars[argsorts(i,0)];
    }

    mus = reord_mus;
    js = reord_js;
    vs = reord_vs;
    covars = reord_covars;



	//hwep = hwepvalue(calls_from_posterior_probs(post_probs));

    //DONE!!!


}














/********************* Main function ****************************/

int main(int argc, char* argv[]){
	
	//cout << argv[1] << endl;
	//cout << argv[2] << endl;
	


	//char * fname; // = "pintacall_mini_test.txt";
	//char * output_fstem; // = "cpp_pinta_output";
	//char * output_prob_ext = ".probs";
	//char * output_call_ext = ".calls";

	string version = "0.8.1";
	cout << "opticall version " << version << endl;
	cout << "thank you for choosing opticall for your genotyping needs" << endl;


	string fname;
	string fname_block = "";
	string output_fstem;
	string info_fname = "";
	string output_prob_ext = ".probs";
	string output_call_ext = ".calls";

	int sample_block_size = 50000;
	
	double resc_threshhold = 5*1e-15;
	double initial_outliers_numsds = 3;
	bool nonorm = false;
	bool noblank = true;
	bool meanintfilter = false;
	bool outliers_only = false;
	string chrom_type = "A";
    bool printblocksample = false;

	if (argc < 5) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
		cout << "Usage is -in <infile> -out <outdir>\n"; // Inform the user of how to use the program
		//cin.get();
		exit(0);
	}


	for (int i = 1; i < argc; i++) { /* We will iterate over argv[] to get the parameters stored inside.
	 * Note that we're starting on 1 because we don't need to know the
	 * path of the program, which is stored in argv[0] */
		//cout << i << endl;
		if (string(argv[i]) == "-in") {
			// We know the next argument *should* be the filename:
			fname = string(argv[i + 1]);
			if (fname_block.compare("") == 0) {
				fname_block = fname;
			}
			i++;
		} else if (string(argv[i]) == "-out") {
			output_fstem = string(argv[i + 1]);
			i++;
		} else if (string(argv[i]) == "-inblock") {
			fname_block = string(argv[i + 1]);
			i++;
		} else if (string(argv[i]) == "-info") {
			cout << "reading in info file" << endl;
			info_fname = string(argv[i + 1]);
			i++;
		} else if (string(argv[i]) == "-hwep") {
			string hwep = string(argv[i + 1]);
			istringstream os(hwep);
			os >> resc_threshhold;
			cout << "rescue threshold is: " << resc_threshhold << endl;
			i++;
		} else if (string(argv[i]) == "-minp") {
			string minp = string(argv[i + 1]);
			istringstream os(minp);
			os >> G_CALL_THRESHOLD;
			cout << "probability call threshold is: " << G_CALL_THRESHOLD << endl;
			i++;
		} else if (string(argv[i]) == "-meanintsds") {
			string meanintsds = string(argv[i + 1]);
			istringstream os(meanintsds);
			os >> initial_outliers_numsds;
			cout << "meanintensity filtering limit is: " << initial_outliers_numsds << " standard deviations" << endl;
			i++;
		} else if (string(argv[i]) == "-nointcutoff") {
			cout << "turning off internal max-intensity cutoff" << endl;
			nonorm = true;
		} else if (string(argv[i]) == "-meanintfilter") {
			cout << "filtering out samples based on mean-intensity across SNPs" << endl;
			meanintfilter = true;
		} else if (string(argv[i]) == "-meanintfilteronly") {
			cout << "optiCall will output samples with outlying mean-intensity across SNPs, then stop. No Calling will be performed." << endl;
			meanintfilter = true;
			outliers_only = true;
		} else if (string(argv[i]) == "-blank") {
			cout << "turning on HWE based SNP blanking" << endl;
			noblank = false;
		} else if (string(argv[i]) == "-noblank") {
			cout << "turning off HWE based SNP blanking" << endl;
			noblank = true;
		} else if (string(argv[i]) == "-X") {
			cout << "X chromosome calling mode" << endl;
			chrom_type = "X";
		} else if (string(argv[i]) == "-Y") {
			cout << "Y chromosome calling mode" << endl;
			chrom_type = "Y";
		} else if (string(argv[i]) == "-MT") {
			cout << "MT calling mode" << endl;
			chrom_type = "MT";
 		} else if (string(argv[i]) == "-pblockdata") {
			cout << "outputting block data" << endl;
			printblocksample = true;           
            
		} else {
			cout << "Not enough or invalid arguments, please try again.\n";
			cout << i << endl;
			//cin.get();
			exit(0);
		}
	}


	

	int snpcount;
	vector<string> individuals;
	snpcount = snpCount(fname.c_str(),true);
	cout << "number of SNPs is " << snpcount << endl;
	individuals = readHeaderInfoFromIntFile(fname.c_str());
	cout << "number of inds is " << individuals.size() << endl;

        int snpcount_block;
        snpcount_block = snpCount(fname_block.c_str(),true);
	
	vector < vector< string > > snpinfo2;

	ofstream out_probsfile ((output_fstem+output_prob_ext).c_str());
	ofstream out_callsfile ((output_fstem+output_call_ext).c_str());

	
	vector<int> excluded_samples;
	MatrixXi samplegenders = MatrixXi::Zero(individuals.size(), 1);
	MatrixXi samplebatches = MatrixXi::Zero(individuals.size(), 1);
	if (info_fname != "")
	{
		//cout << "getting info file details" << endl;
		excluded_samples = readInfoFile(info_fname, individuals, samplegenders, samplebatches);
                cout << "after info file exclusion we have " << individuals.size()-excluded_samples.size() << " samples." << endl;
		//cout << "got info file details" << endl;
	}
	
    
    //cout << "sample batches "  << endl;
    
    //cout << samplebatches << endl;
    
	
    

	vector<int> initial_outliers; 
	if (meanintfilter)
	{
		initial_outliers = findIntensityOutliers(fname, snpcount, individuals, initial_outliers_numsds, excluded_samples);
		if (outliers_only)
		{
			//just list the outliers and quiet optiCall - no calling to be performed.
			exit(0);
		}
	}
	

	

	
	
	
	//distance to define outliers
	double outlier_range;

	//load data into mixture model
	MatrixXd blockmus;
	vector<MatrixXd> blockcovars;
    MatrixXd blockjs;
    MatrixXd blockvs;

	MatrixXd vs(4,1);
	vector<MatrixXd> custommus;
	vector<MatrixXd> customcovars;
	vs << 1,1,1,1;
	custommus.push_back(Matrix<double, 1, 2>::Zero());
	customcovars.push_back(Matrix<double, 2, 2>::Identity() * 100	);

	MatrixXd sd_sample;
    
    bool successful_block = false;

	/*******SAMPLING & Block step*******/
	/********************************/
	int blockattempts = 0;
	do {
                cout << "file being used for block is " << fname_block << endl;
                //cout << "number of SNPS in blockfile is " << snpcount_block << endl;
		MatrixXd sample  =  fetchRandomIntensities(fname_block, sample_block_size,  snpcount_block, individuals, combineExclusionLists(initial_outliers,excluded_samples)    );
		
		//only transform the sample during the rescue stage
		//sample = transform_data(sample);
		vector<int> sample_nan_inds;
		//cout << "the sample is" << endl;
		//cout << sample << endl;
		vector<int> dummy_outliers;
		vector<int> dummy_excluded_samples;
        //don't use gender information here and so create a set of dummy genders
		MatrixXi dummy_genders = MatrixXi::Zero(sample.size(), 1);;
		MatrixXi dummy_batches = MatrixXi::Zero(sample.size(), 1);;
 		sample = strip_nans(sample, sample_nan_inds,nonorm,outlier_range,true, dummy_outliers, dummy_excluded_samples,dummy_genders, dummy_batches);
		
        
        if (sample.rows() == sample_nan_inds.size())
        {
            cout << "Only NANs found while creating prior information" << endl;
            blockattempts++;
            continue;
        }
        
        
        
        cout << "NaNs & outliers removed: " << sample_nan_inds.size() << endl;
		cout << "---------" << endl;
		
		//vector<int> sample_outl_inds;
		//sample = strip_outliers(sample, sample_outl_inds);
		
		//cout << "Outliers removed: " << sample_outl_inds.size() << endl;
		//cout << "---------" << endl;
		
		//cout << sample << endl;
		sd_sample = sd_of_data(sample);
		cout << "sd sample " << sd_sample << endl;
		cout << "generating priors..." << endl;
        
        
        if (printblocksample) 
        {
            //should print out the sample here
            ofstream out_sample ((output_fstem+".blockdata").c_str());
            for (int p = 0; p < sample.rows();p++)
            {
                out_sample << sample.row(p) << "\n";
            }
            out_sample.close();
        }
        
        
		genotype_mm_block_caller(sample, vs, custommus, customcovars, blockmus, blockcovars, blockjs, blockvs,chrom_type);
		//MatrixXi blockmap_arranging = MatrixXi::Zero(1, sample.rows());
		arrange_mixture_components(sample , blockmus,  blockcovars,  blockjs,  blockvs,chrom_type);

		blockattempts++;

		cout << "blocking attempts: " << blockattempts << endl;
		
		
		cout << "priors - Mus" << endl;
		cout << blockmus << endl;
		cout << "priors - Covars" << endl;
		for (int i = 0; i < blockcovars.size(); i++)
		{
			cout << blockcovars[i] << endl;
		}
		cout << "priors - js" << endl;
		cout << blockjs << endl;
		cout << "priors - vs" << endl;
		cout << blockvs << endl;

		if (   ! (isnan(blockmus.sum()) || isinf(blockmus.sum()) || blockjs.row(0).sum() == 0 || blockjs.row(1).sum() == 0 || blockjs.row(2).sum() == 0 )  )
		{
			cout << "prior information created successfully" << endl;
            successful_block = true;
			break;
		}

	} while (blockattempts <= 5);

	if ( ! successful_block || isnan(blockmus.sum()) || isinf(blockmus.sum()) || blockjs.row(0).sum() == 0 || blockjs.row(1).sum() == 0 || blockjs.row(2).sum() == 0   )
	{
		cout << "could not create priors from data following multiple attempts. Please check dataset & maybe try again. Exiting..." << endl;
		exit(0);
	}

	/*******END OF SAMPLING & Block step*******/
	/********************************/





	/**
	 *STEP 2: go through the file line by line - and call each of the SNPs
	 */
    //reopen the intensity file
	ifstream intensityfile_reopen (fname.c_str());
	if (intensityfile_reopen.is_open()  && intensityfile_reopen.good())
	{
		string header;
		getline(intensityfile_reopen,header);
	}
	
	//update the individuals based on the exclude list
	vector<string> individualstocall = individualsForCalling(individuals, excluded_samples);
	outputtitleinfo(out_callsfile, out_probsfile, individualstocall);

	for (int l = 0; l < snpcount; l++)
	{
		MatrixXd lineblock = readVblockFromIntFile(intensityfile_reopen,1,individuals.size(),snpinfo2,0);	//read one line from the file
		//MatrixXi blockmap = MatrixXi::Zero(1, individuals.size());
		//cout << lineblock << endl;
		MatrixXd line_vs(4,1);
		vector<MatrixXd> line_custommus;
		vector<MatrixXd> line_customcovars;
		line_vs << 1,1,1.3,1;
		line_custommus.push_back(Matrix<double, 1, 2>::Zero());
		line_customcovars.push_back(Matrix<double, 2, 2>::Identity() * 100);


		int dim = 2;
        int numclasses = 4;
		int data_count = lineblock.rows()*lineblock.cols()/2;
		MatrixXd int_data_T(dim,data_count);
		MatrixXd int_data;
		int_data_T = Map<MatrixXd>(lineblock.data(),dim,data_count);
		int_data = int_data_T.transpose();


		MatrixXd int_data_w_nans = int_data;
		vector<int> nan_samples;
		
		//double mean_distance = int_data.rowwise().norm().mean();
		//double exclusion_range = mean_distance+outlier_range;
		
		//need to completely remove excluded samples here
		//int_data_wo_excluded = 
		MatrixXi snp_genders = samplegenders;
        MatrixXi snp_batches = samplebatches;
        
        cout << endl;
        cout << "--------------------" << endl;
        cout << "calling SNP: " << snpinfo2[l][0] << endl;
        
		//int_data = strip_nans(int_data, nan_samples,nonorm,outlier_range,false,initial_outliers,excluded_samples,snp_genders);
        
        //cout << "int data size is " << int_data.rows() << endl;
        
        int_data = strip_nans(int_data, nan_samples,true,outlier_range,false,initial_outliers,excluded_samples,snp_genders, snp_batches);
		
        
        
		/*
		cout << "data comp" << endl;
		int p_counter = 0;
		for (int i = 0; i < int_data_w_nans.rows(); i++)
		{
		
			if (nan_samples.size() > 0  && find (nan_samples.begin(), nan_samples.end(), i)  !=   nan_samples.end())
			{

			
			}
			else if (excluded_samples.size() > 0  && find (excluded_samples.begin(), excluded_samples.end(), i)  !=   excluded_samples.end())
			{
				
				
			}
			else
			{
				cout << int_data_w_nans(i,0) << " " << int_data_w_nans(i,1) << " : " << int_data(p_counter,0) << " " << int_data(p_counter,1) << endl; 
				if (int_data_w_nans(i,0) != int_data(p_counter,0) || int_data_w_nans(i,1) != int_data(p_counter,1)  )
				{
					cout << "EXCLUSION ERROR" << endl;
				}
				p_counter++;
			}
				
			
		}
		cout << "end data comp" << endl;
		*/
		
        //strip any outlying intensities from the inference part of the caller
        vector<int> line_mm_removed_indices;
        MatrixXd inference_int_data = int_data;
        MatrixXi inference_snp_genders = snp_genders;
        if (!nonorm && nan_samples.size() + excluded_samples.size() != data_count)
        {
            inference_int_data = strip_from_line_inference(int_data, line_mm_removed_indices, outlier_range, inference_snp_genders);
        }
        else{
            cout << "Caught empty data" << endl;
        }
        
		clock_t linetStart = clock();


		if (nan_samples.size() + excluded_samples.size() == data_count) {
			//all the data for this SNP is NAN
			cout << "all intensity data for snp is NaN, calling everything unknown"  << endl;;
			MatrixXd post_probs = probs_with_nans(4, excluded_samples ,int_data_w_nans.rows() );
			//cout << post_probs << endl;
			//then output the posterior probs to file
			outputpostprobs(post_probs,out_probsfile,snpinfo2[l],chrom_type);
			outputcalls(post_probs,out_callsfile,snpinfo2[l],chrom_type);

			cout << "line step took " << (double)(clock() - linetStart)/CLOCKS_PER_SEC  << endl;

			//go to the next SNP
			continue;
		}


		if (inference_int_data.norm() == 0 || sd_of_data(inference_int_data).norm() == 0) {
			//all the data for this SNP is 0
			cout << "too many zeros in intensity data, calling everything unknown"  << endl;;
			MatrixXd post_probs = probs_with_nans(4, excluded_samples, int_data_w_nans.rows() );
			//cout << post_probs << endl;
			//then output the posterior probs to file
			outputpostprobs(post_probs,out_probsfile,snpinfo2[l],chrom_type);
			outputcalls(post_probs,out_callsfile,snpinfo2[l],chrom_type);
			
			cout << "line step took " << (double)(clock() - linetStart)/CLOCKS_PER_SEC  << endl;
			
			//go to the next SNP
			continue;
		}
		
        
        //final line MM parameters
        MatrixXd linemus;
        vector<MatrixXd> linecovars;
        MatrixXd linejs;
        MatrixXd linevs;
		genotype_mm_line_caller_w_priors(inference_int_data, line_vs, line_custommus, line_customcovars, blockmus, blockcovars, blockjs, blockvs, sd_sample, linemus,  linecovars,  linejs,  linevs, chrom_type, inference_snp_genders);
        MatrixXd post_probs =  probs_j_line(dim,numclasses,int_data,linemus, linecovars,linejs, linevs, blockmus, blockcovars,blockjs, blockvs);
        
        
		//analyse these probs for HWE-p
		//go from probs to calls

		if (chrom_type == "A")
		{
		
			double hwep = hwepvalue(calls_from_posterior_probs(post_probs), snp_batches);
			cout << "HWEP " << snpinfo2[l][0] << " " << hwep << endl;


			/****
			 * STEP 3 - remove sampled contour if HWE-p test failed
			 */
		
			//5*1e-15

			if (  isnan(hwep) || hwep < resc_threshhold) {
				cout << "Out of HWE, running Rescue" << endl;


				//=============================
				//BEGINNING OF RESCUE STEP - to fix shifted intensities


				MatrixXd line_data = transform_data(int_data);
                MatrixXd inference_line_data = transform_data(inference_int_data);

				MatrixXd blockmus_ill_fix;
				vector<MatrixXd> blockcovars_ill_fix;
				MatrixXd blockjs_ill_fix;
				MatrixXd blockvs_ill_fix;

				MatrixXd vs_ill_fix(4,1);
				vector<MatrixXd> custommus_ill_fix;
				vector<MatrixXd> customcovars_ill_fix;
				vs_ill_fix << 1,1,1.3,1;
				custommus_ill_fix.push_back(Matrix<double, 1, 2>::Zero());
				customcovars_ill_fix.push_back(Matrix<double, 2, 2>::Identity() * 100);


				//run a mixture model to get back the distributions


				//genotype_mm_block_caller(line_data, vs_ill_fix, custommus_ill_fix, customcovars_ill_fix, blockmus_ill_fix, blockcovars_ill_fix, blockjs_ill_fix, blockvs_ill_fix);


				clock_t tStart = clock();
				//cout << "rescue calling step" << endl;
				genotype_mm_rescue_caller(inference_line_data, vs_ill_fix, custommus_ill_fix, customcovars_ill_fix,
					blockmus_ill_fix,  blockcovars_ill_fix,  blockjs_ill_fix,  blockvs_ill_fix);
				cout << "rescue step took " << (double)(clock() - tStart)/CLOCKS_PER_SEC  << endl;

				//cout << "===============" << endl;
				//cout << blockmus_ill_fix << endl;
				//cout << blockjs_ill_fix << endl;
				//cout << "===============" << endl;

				arrange_mixture_components(line_data, blockmus_ill_fix,  blockcovars_ill_fix,  blockjs_ill_fix,  blockvs_ill_fix, chrom_type);
				//cout << "mixture components arranged" << endl;
                post_probs = probs_j_rescue(line_data,blockmus_ill_fix, blockcovars_ill_fix, blockjs_ill_fix, blockvs_ill_fix);
				//cout << "mixture components arranged and post_probs calculated" << endl;

				hwep = hwepvalue(calls_from_posterior_probs(post_probs),snp_batches);
				cout << "HWEP after Rescue " << snpinfo2[l][0] << " " << hwep << endl;

				if ((isnan(hwep) || hwep < resc_threshhold)) {
				
					if ( noblank == false) {
						cout << " rescue can't fix it! Calling everything unknown" << endl;
						post_probs = postprobstounknown(post_probs);
					}
					else {
						cout << "SNP would be set to unknown if -noblank was not applied" << endl;
					}
				}

			}

		}


		//reinflate data to set the nan rows to unknown
		//cout << "reinflating outputting post probs " << endl;
		post_probs = probs_with_nans(post_probs,nan_samples, excluded_samples, int_data_w_nans.rows() );
		//cout << "reinflation doesn't cause assertion error" <<  endl;

		//then output the posterior probs to file
		outputpostprobs(post_probs,out_probsfile,snpinfo2[l], chrom_type);
		outputcalls(post_probs,out_callsfile,snpinfo2[l], chrom_type);
		//cout << "segfault doesn't happen here" << endl;

		cout << "line step took " << (double)(clock() - linetStart)/CLOCKS_PER_SEC  << endl;
	}
	intensityfile_reopen.close();





	out_probsfile.close();
	out_callsfile.close();

	//readFromIntFile("pintacall_mini_test.txt");
	
	
	

	
	
	
	
}



