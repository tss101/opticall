/*
 *  filtering.h
 *  
 *
 *  Created by Tejas Shah on 15/11/2011.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */


#include "inclusions.h"



#ifndef FILTERING_H
#define FILTERING_H

bool lessthanfunction (int i,int j);
unsigned int snpCount(const char * filename, bool header); 
std::vector<std::string> readHeaderInfoFromIntFile(const char * fname); 
Eigen::MatrixXd readVblockFromIntFile(std::ifstream & myfile, int vertical_block_size, int numsamples, std::vector < std::vector <std::string>  > &snpinfo, int offset);
std::vector<std::string> individualsForCalling(std::vector<std::string> individuals, std::vector<int> excluded);
std::vector<int> findIntensityOutliers(std::string fname, int snpCount, std::vector<std::string> individuals,double numsds, std::vector<int> exclude_list);
Eigen::MatrixXd fetchRandomIntensities(std::string fname, int sample_block_size, int snpcount, std::vector<std::string> individuals, std::vector<int> initial_outliers);
std::vector<int> readInfoFile(std::string fname, std::vector<std::string> allsamples, Eigen::MatrixXi &samplegenders, Eigen::MatrixXi &samplebatches );
std::vector<int> combineExclusionLists(std::vector<int> list1, std::vector<int> list2);


#endif